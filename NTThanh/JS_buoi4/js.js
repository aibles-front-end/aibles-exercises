axios.defaults.baseURL = 'https://60becf8e6035840017c17a48.mockapi.io/api';

function getData() {
    axios.get('/users')
        .then(function (response) {
            appendData(response.data)
        })
        .catch(function (error) {
            console.log(error);
        });
}
getData();

function appendData(users) {
    const tbody = document.getElementById("tbody");
    tbody.innerHTML = '';
    users.map((user, index) =>
        tbody.innerHTML += (`
        <tr key=${index}>
            <th scope="row">${user.id}</th>
            <td>${user.name}</td>
            <td>${user.email}</td>
            <td>${user.city}</td>
            <td><button 'type="button" class="btn btn-info" onclick="displayEdit(${user.id})"'>Edit</button></td>
            <td><button 'type="button" class="btn btn-danger" onclick="handleDeleteUser(${user.id})"'> x </button></td>
        </tr>`));
}

const user = document.getElementsByClassName("form-control");
function handleAddUser() {
    const name = user[0].value
    const email = user[1].value
    const city = user[2].value
    if (name.trim()==0 || email.trim()==0 || city.trim()==0) {
        alert("You must complete the input !!!");
    }
    else {
        axios.post('/users', {
            name: name,
            email: email,
            city: city
        })
            .then(function (response) {
                getData()
            })
            .catch(function (error) {
                alert(error);
            });
    }
    alert("Create Succesfully!");
}
//XÓA
function handleDeleteUser(id){
    if(confirm("You want Delete?")){
        axios.delete('/users/' + id)
            .then(function (response) {
                getData()
                //alert(response);
            })
            .catch(function (error) {
                alert(error);
            });
    }
}

//EDIT
function displayEdit(id){
    const table = document.getElementById('table1');
    for(var i = 1; i < table.rows.length; i++){
        table.rows[i].onclick = function(){
            //id=this.cells[0].innerHTML;
            document.getElementById("editName").value = this.cells[1].innerHTML;
            document.getElementById("editEmail").value = this.cells[2].innerHTML;
            document.getElementById("editCity").value = this.cells[3].innerHTML;
        };
    }
        document.querySelector("#editUser").style.display="block";
        document.querySelector("#createUser").style.display="none";
        document.querySelector("#home").style.display="none";

    document.querySelector("#btnSave").addEventListener("click",
     function handleEditUser() {       
        const name = user[3].value;
        const email = user[4].value;
        const city = user[5].value;
        if (name.trim()==0 || email.trim()==0 || city.trim()==0) {
            alert("You must complete the input !!!");
        }
        axios.put('/users/'+id, {
            name: name,
            email: email,
            city: city,
        })
            .then(function (response) {
                getData()
        })
            .catch(function (error) {
                alert(error);
            });
        alert("Edit Succesfully!");
        document.querySelector("#editUser").style.display="none";
        document.querySelector("#createUser").style.display="block";
        document.querySelector("#home").style.display="block";
    });
}
    


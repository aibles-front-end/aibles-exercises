import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import './Component-Thanh/Content/Content.css'
// import './Component-Thanh/Header/Header.css'
// import './Component-Thanh/Sidebar/Sidebar.css'
import 'antd/dist/antd.css';

import Header from './Component-Thanh/Header/Header.jsx';
import Content from './Component-Thanh/Content/Content.jsx';
import Sidebar from './Component-Thanh/Sidebar/Sidebar.jsx';

import {Layout } from 'antd';



function App () {
  const [collapsed, setCollapsed] = useState(false);
  
  const toggle = () => {
    setCollapsed(!collapsed);
  };

  return (
    <Layout>
        <Sidebar collapsed={collapsed}/>  
        <Layout className="site-layout">
          <Header toggle={toggle} collapsed={collapsed}/>    
          <Content/>
        </Layout>
    </Layout>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));


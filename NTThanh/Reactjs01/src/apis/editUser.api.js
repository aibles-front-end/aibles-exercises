import axios from "axios";
axios.defaults.baseURL = "https://60becf8e6035840017c17a48.mockapi.io/api";

export function editData(value, id) {
  return axios.put("/users/" + id, {
    name: value.name,
    city: value.city,
    email: value.email,
  });
}

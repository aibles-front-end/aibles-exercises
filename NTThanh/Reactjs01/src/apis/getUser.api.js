import axios from "axios";
axios.defaults.baseURL = "https://60becf8e6035840017c17a48.mockapi.io/api";

export function getData() {
  return axios.get("/users");
}

export function createData(value) {
  return axios.post("/users", {
    id: value.id,
    name: value.name,
    city: value.city,
    email: value.email,
  });
}
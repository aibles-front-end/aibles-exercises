import axios from "axios";
axios.defaults.baseURL = "https://60becf8e6035840017c17a48.mockapi.io/api";

export function deleteData(id) {
  return axios.delete("/users/" + id);
}

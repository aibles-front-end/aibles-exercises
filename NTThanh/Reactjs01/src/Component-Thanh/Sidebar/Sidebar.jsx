import React from 'react';
import AVATAR from '../../avatar.JPG';
import 'antd/dist/antd.css';
import './Sidebar.css';
import { Layout, Menu, Input} from 'antd';
import {
  BlockOutlined,
  AppstoreOutlined,
  PieChartOutlined,
  DashboardOutlined,
  DesktopOutlined, 
  FormOutlined,
  TableOutlined,
  CalendarOutlined,
  MailOutlined,
  BuildFilled,
  FolderFilled,
  FolderOpenFilled,
} from '@ant-design/icons';
import{FaSearch, FaRegCircle} from "react-icons/all"; 
const {Sider} = Layout; 
const { SubMenu } = Menu;

function Siderbar ({collapsed}){
    return(
        <Sider breakpoint="md" trigger={null} collapsedWidth="50" width="250" collapsed={collapsed} className="Siderbar">
            {!collapsed ? 
            <div className="sider_ver1" >
                <div className="logo"><b>Admin</b> LTE  </div> 
                <div className="sider_intro">
                    <div className="sider_intro_avar"><img src={AVATAR} alt="" /></div>
                    <div className="sider_intro_name">
                        <div className="intro_name-name">Nguyen Thanh</div>
                        <div className="intro_name-online">
                            <i className="fa fa-circle"></i>
                            <div>Online</div>
                        </div>
                    </div>
                </div>
                <div className="sider_button">
                    <input className="search-input" placeholder="Search..."></input>
                    <span className="search-icon"><FaSearch /></span>
                </div>
            </div> :
            <div className="sider_ver2">
                <div className="logo" style={{width:'51px', fontSize:'20px'}}><b>A</b>LT</div>
                <div className="sider_intro_avar2"><img src={AVATAR} alt="" /></div>
            </div>}
            {!collapsed ?<div className="lable1">MAIN NAVIGATION</div> : <div className="lable2"></div>}
            <Menu mode="inline" theme="dark" collapsed={collapsed} className="sider"> 
                <SubMenu key="sub1" icon={<DashboardOutlined className="DashboardOutlined" />} title="Dashboard">             
                    <Menu.Item key="1" icon={<FaRegCircle className="FaRegCircle"/> }  >Dashboard v1</Menu.Item>
                    <Menu.Item key="2"icon={<FaRegCircle className="FaRegCircle"/>}>Dashboard v2</Menu.Item>
                </SubMenu>
                <Menu.Item key="3" icon={<BlockOutlined />}>Layout Options</Menu.Item>
                <Menu.Item key="4" icon={<AppstoreOutlined />}>Widgets</Menu.Item>
                <SubMenu key="sub2" icon={<PieChartOutlined />} title="Charts">             
                    <Menu.Item key="5" icon={<FaRegCircle className="FaRegCircle"/> }  >Chart</Menu.Item>
                    <Menu.Item key="6"icon={<FaRegCircle className="FaRegCircle"/>}>Morris</Menu.Item>
                    <Menu.Item key="7"icon={<FaRegCircle className="FaRegCircle"/>}>Flot</Menu.Item>
                    <Menu.Item key="8"icon={<FaRegCircle className="FaRegCircle"/>}>Inline charts</Menu.Item>
                </SubMenu>
                <SubMenu key="sub3" icon={<DesktopOutlined />} title="UI Elements">             
                    <Menu.Item key="9" icon={<FaRegCircle className="FaRegCircle"/> }>General</Menu.Item>
                    <Menu.Item key="10"icon={<FaRegCircle className="FaRegCircle"/>}>Icons</Menu.Item>
                    <Menu.Item key="11"icon={<FaRegCircle className="FaRegCircle"/>}>Buttons</Menu.Item>
                    <Menu.Item key="12"icon={<FaRegCircle className="FaRegCircle"/>}>Siders</Menu.Item>
                    <Menu.Item key="13"icon={<FaRegCircle className="FaRegCircle"/>}>Timeline</Menu.Item>
                    <Menu.Item key="14"icon={<FaRegCircle className="FaRegCircle"/>}>Modals</Menu.Item>
                </SubMenu>
                <SubMenu key="sub4" icon={<FormOutlined />} title="Forms">             
                    <Menu.Item key="15" icon={<FaRegCircle className="FaRegCircle"/> }>General Elements</Menu.Item>
                    <Menu.Item key="16"icon={<FaRegCircle className="FaRegCircle"/>}>Advanced Elements</Menu.Item>
                    <Menu.Item key="17"icon={<FaRegCircle className="FaRegCircle"/>}>Editors</Menu.Item>
                </SubMenu>
                <SubMenu key="sub5" icon={<TableOutlined />} title="Tables">             
                    <Menu.Item key="18" icon={<FaRegCircle className="FaRegCircle"/> }>Simple Tables</Menu.Item>
                    <Menu.Item key="19"icon={<FaRegCircle className="FaRegCircle"/>}>Data Tables</Menu.Item>
                </SubMenu>
                <Menu.Item key="20" icon={<CalendarOutlined />}>Calendar</Menu.Item>
                <Menu.Item key="21" icon={<MailOutlined />}>Mailbox</Menu.Item>
                <SubMenu key="sub6" icon={<FolderFilled />} title="Examples">             
                    <Menu.Item key="22" icon={<FaRegCircle className="FaRegCircle"/> }>Invoice</Menu.Item>
                    <Menu.Item key="23"icon={<FaRegCircle className="FaRegCircle"/>}>Profile</Menu.Item>
                    <Menu.Item key="24"icon={<FaRegCircle className="FaRegCircle"/>}>Login</Menu.Item>
                    <Menu.Item key="25"icon={<FaRegCircle className="FaRegCircle"/>}>Register</Menu.Item>
                    <Menu.Item key="26"icon={<FaRegCircle className="FaRegCircle"/>}>Lockcreen</Menu.Item>
                </SubMenu>
                <SubMenu key="sub7" icon={<BuildFilled />} title="Multilevel">             
                    <Menu.Item key="27" icon={<FaRegCircle className="FaRegCircle"/> }>Level One</Menu.Item>
                    <Menu.Item key="28"icon={<FaRegCircle className="FaRegCircle"/>}>Level Two</Menu.Item>
                </SubMenu>
                <Menu.Item key="29" icon={<FolderOpenFilled />}>Documentation</Menu.Item>
          </Menu>
          {!collapsed ?<div className="lable1">LABLES</div> : <div className="lable2"></div>}
          <Menu mode="inline" theme="dark" collapsed={collapsed} className="sider">
                <Menu.Item key="31"icon={<FaRegCircle className="Important"/> } >Important</Menu.Item>
                <Menu.Item key="32"icon={<FaRegCircle className="Warning"/>}>Warning</Menu.Item>
                <Menu.Item key="33"icon={<FaRegCircle className="Information"/>}>Information</Menu.Item>
          </Menu>
        </Sider>
    );
}
export default Siderbar
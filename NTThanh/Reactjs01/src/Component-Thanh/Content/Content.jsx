import React from 'react';
import Tables from '../Tables/Tables.jsx';
import './Content.css'
import { AiFillDashboard, AiOutlineRight } from "react-icons/all";
function Content (){
    return(
        <div className="content">
            <div className="content_name">
                <div className="title">
                    <span className="title_bold">Data Tables </span>
                    <span className="title_light ">  advanced tables</span>
                </div>

                <div className="path">
                    <span className="sub-path path-icon">
                    <AiFillDashboard />
                    </span>
                    <span className="sub-path path-text"> Home</span>
                    <span>></span>
                    <span className="sub-path path-text">Tables</span>
                    <span>></span>
                    <span className="sub-path path-light">Data tables</span>
                </div>
            </div>
            <div className="content_table">
                {/* <p className="content_table-name">Hover Data Table</p> */}
                <Tables className="content_table-table"/>
            </div>

        </div>
    )
}
export default Content
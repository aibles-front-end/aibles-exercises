import React from 'react';
import './Header.css'
import {
    MenuOutlined,
    MailOutlined, 
    BellOutlined,
    FlagOutlined, 
    SettingFilled
  } from '@ant-design/icons';
import AVATAR from '../../avatar.JPG';
function Header ({toggle, collapsed}){
    return (
      <div className="header"  collapsed={collapsed}>
           <div className="logo_ver2">AdminTLE</div>
           <div className="header_content">
                <div className="header_menuOutline">
                    {React.createElement(MenuOutlined , {className: 'trigger',onClick: toggle})}
                </div>
                <div className="header_icon">
                    <MailOutlined className="header_icon-icon"/>
                    <BellOutlined className="header_icon-icon"/>
                    <FlagOutlined className="header_icon-icon"/>
                    <div className="header_img"><img src={AVATAR}/></div>
                    <div className="header_name">Nguyen Thanh</div>
                    <SettingFilled className="header_icon-icon"/> 
                </div>   
           </div>
                     
       </div>
    ); 
}
export default Header


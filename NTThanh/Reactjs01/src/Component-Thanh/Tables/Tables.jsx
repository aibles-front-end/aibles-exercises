// import {Table} from 'antd';

// function Tables(){
//     const names=['Nguyen Thi Thanh', 'Mai Phuong Thao', 'Tran Thanh Hai', 'Hoang Hai', 'Manh Hung',
//                  'Duong Thanh Nga', 'Duong Quy', 'Trieu Le Dinh', "Ton Le"];
//     const sexs=['female', 'male'];
//     const cities=['Thanh Hoa', 'Hai Phong', 'Ha Noi', 'Bac Giang', 'TP Ho Chi Minh',
//                  'Ben Tre', 'Ca Mau'];
//     const columns=[
//         {
//           title: 'ID',
//           dataIndex: 'id',
//         },
//         {
//           title: 'Name',
//           dataIndex: 'name',
//         },
//         {
//           title: 'City',
//           dataIndex: 'city',
//         },
//         {
//           title: 'Email',
//           dataIndex: 'email',
//         },
//         {
//           title: 'Option',
//           dataIndex: 'option',
//         }
//       ];
 
//      const dataSource = [];
//     for(let i = 0; i < 50; i++){
//       let indexName = Math.floor(Math.random()*names.length);
//       let indexSex = Math.floor(Math.random()*sexs.length);
//       let indexCity = Math.floor(Math.random()*cities.length);
//         dataSource.push({
//             id: '000'+i,
//             name: names[indexName],
//             email: names[indexName].toLowerCase().replace(/\s/g, '')+'@gmail.com',
//             sex: sexs[indexSex],            
//             city: cities[indexCity]
//         });
//     }
//     return(
//         <Table columns={columns} dataSource={dataSource} position="[bottomCenter]" className="tables"/>
//     );
// }
// export default Tables
import ReactDOM from 'react-dom'; 
import { useEffect, useState } from "react";
import {
  BiEdit,
  FaSortAmountDownAlt,
  MdDelete,
  RiArrowUpDownFill,
} from "react-icons/all";
import { Button } from "antd";
import { getData } from "../../apis/getUser.api";
import InputModal from "../InputModal/InputModal";
import ConfirmModal from "../ConfirmModal/ConfirmModal";
import "./Tables.css"
import { Pagination } from 'antd';


function Tables(){
  const [src, setSrc] = useState([]);
  const [create, setCreate] = useState(false);
  const [edit, setEdit] = useState(null);
  const [del, setDel] = useState(null);

  useEffect(() => {
    getData()
      .then((res) => setSrc(res.data))
      .catch((error) => console.log(error));
  }, []);

  const data = src.map((user, index) => {
    return (
      <tr  key={index}>
        <td>{user.id}</td>
        <td>{user.name}</td>
        <td>{user.city}</td>
        <td>{user.email}</td>
        <td>
          <span className="icon_edit" 
            onClick={() => {
              setEdit(user);
            }}>
            <BiEdit />
          </span>
          <span className="icon_delete"
            onClick={() => {
              setDel(user);
            }}>
            <MdDelete />
          </span>
        </td>
      </tr>
    );
  });
  function itemRender(current, type, originalElement) {
    if (type === 'prev') {
      return <a className='prev'>Previous</a>;
    }
    if (type === 'next') {
      return <a className='prev'>Next</a>;
    }
    return originalElement;
  }
  return (
    <div className="Tables_content">
      <div className="Tables_content_header">
        <span className="title">Hover Data Table</span>
        <Button type="primary" className="btn"
          onClick={() => {
            setCreate(true);
          }}> 
          CREATE USER </Button>
      </div>

      <div className="wrap_table">
        <table className="table">
          <thead>
            <tr>
              <th >
                <span className="th_title">ID</span>
                <span className="th_icon">
                  <FaSortAmountDownAlt />
                </span>
              </th>

              <th >
                <span className="th_title">Name</span>
                <span className="th_icon">
                  <RiArrowUpDownFill />
                </span>
              </th>

              <th >
                <span className="th_title">City</span>
                <span className="th_icon">
                  <RiArrowUpDownFill />
                </span>
              </th>

              <th >
                <span className="th_title">Email</span>
                <span className="th_icon">
                  <RiArrowUpDownFill />
                </span>
              </th>

              <th >
                <span className="th-title">Option</span>
                <span className="th-sort">
                  <RiArrowUpDownFill />
                </span>
              </th>
            </tr>
          </thead>

          <tbody>{data}</tbody> 

          {/* <tfoot>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>City</th>
              <th>Email</th>
              <th>Option</th>
            </tr>
          </tfoot> */}
        </table>
      </div>

      <div className="table_foot">
        <span className="table_infor">
          Showing 1 to {src.length} of {src.length} users
        </span>
        <table className="paging">
            <Pagination defaultCurrent={1} total={50} itemRender={itemRender}/>
        </table>
      </div>
      <InputModal
        create={create}
        setCreate={setCreate}
        edit={edit}
        setEdit={setEdit}
        setSrc={setSrc}
      />

      <ConfirmModal del={del} setDel={setDel} setSrc={setSrc} />
  </div>
  );
};

export default Tables;


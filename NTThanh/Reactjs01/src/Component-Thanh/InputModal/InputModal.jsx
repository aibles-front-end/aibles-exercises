import { Modal, Form, Input, Button, notification } from "antd";
import { editData } from "../../apis/editUser.api";
import { createData, getData } from "../../apis/getUser.api";
import "./InputModal.css";

const InputModal = ({ create, setCreate, edit, setEdit, setSrc }) => {
  const onFinish = (value) => {
    if (edit) {
      setEdit(null);
      editData(value, edit.id)
        .then(() => displayData())
        .catch(() =>
          notification["error"]({
            message: "User Edit Failed",
            placement: "bottomLeft",
          })
        );
    } else {
      setCreate(false);
      createData(value)
        .then(() => displayData())
        .catch(() =>
          notification["error"]({
            message: "User Create Failed",
            placement: "bottomLeft",
          })
        );
    }
  };

  const displayData = () => {
    getData()
      .then((res) => setSrc(res.data))
      .then(() => {
        notification["success"]({
          message: edit
            ? "User Edited Successfully"
            : "User Created Successfully",
          placement: "bottomLeft",
        });
        edit ? setEdit(null) : setCreate(false);
      })
      .catch((error) => console.log(error));
  };

  return (
    <Modal
      visible={create || edit}
      onCancel={() => {
        setCreate(false);
        setEdit(null);
      }}
      footer={[]}
      destroyOnClose={true}
    >
      <Form
        name="basic"
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 18 }}
        onFinish={onFinish}
        initialValues={edit || { name: "", email: "", city: "" }}
      >
        <h2>{edit ? "Edit user" : "Create user"}</h2>

        <Form.Item
          label="Name"
          name="name"
          rules={[{ required: true, message: "Please input the name" }]}
        >
          <Input className="inp-text" />
        </Form.Item>

        <Form.Item
          label="City"
          name="city"
          rules={[{ required: true, message: "Please input the city" }]}
        >
          <Input className="inp-text" />
        </Form.Item>

        <Form.Item
          label="Email"
          name="email"
          rules={[{ required: true, message: "Please input the email" }]}
        >
          <Input className="inp-text" />
        </Form.Item>

        <div className="btn-form">
          <Button
            className="btn-form-foot"
            onClick={() => {
              setCreate(false);
              setEdit(null);
            }}
          >Cancel
          </Button>
          <Button className="btn-form-foot" type="primary" htmlType="submit">
            {edit ? "Save" : "Add"}
          </Button>
        </div>
      </Form>
    </Modal>
  );
};

export default InputModal;

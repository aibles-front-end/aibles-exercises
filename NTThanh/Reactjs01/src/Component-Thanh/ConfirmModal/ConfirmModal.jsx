import { Modal, notification } from "antd";
import { deleteData } from "../../apis/deleteUser.api";
import { getData } from "../../apis/getUser.api";
import "./ConfirmModal.css";

const ConfirmModal = ({ del, setDel, setSrc }) => {
  const handleOK = () => {
    deleteData(del.id)
      .then(() => displayData())
      .catch(() =>
        notification["error"]({
          message: "User Delete Failed",
          placement: "bottomLeft",
        })
      );
  };

  const displayData = () => {
    setDel(null);
    getData()
      .then((res) => setSrc(res.data))
      .then(() => {
        notification["success"]({
          message: "User Delete Successfully",
          placement: "bottomLeft",
        });
      })
      .catch((error) => console.log(error));
  };

  return (
    <Modal
      visible={del}
      okText="OK"
      onOk={handleOK}
      onCancel={() => {
        setDel(null);
      }}
    >
      Do you want to delete this user?
    </Modal>
  );
};

export default ConfirmModal;

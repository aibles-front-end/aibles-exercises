
const newArr = (arr)=>{
    return arr.reduce(function(output, item){
       return output.concat(Array.isArray(item) ? newArr(item) : item);       
    }, []);
}

const getLength = (x) =>{
    console.log(newArr(x).length);
}
getLength([1, [2], 1, [2], 1]);

/*
function flatten(arr) {
  return arr.reduce(function (flat, toFlatten) {
    return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten);
  }, []);
}
*/




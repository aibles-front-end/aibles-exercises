//3. Create a function that takes an array of numbers and return "Boom!" if the number 7 appears in the array. 
//Otherwise, return "there is no 7 in the array".
var arrA = [1,2,33,47,56,68];
var arrB = [1,2,33,45,68];
const checkSeven = (arr)=>{
    var check = false;
    for (const i of arr) {
        var x = i.toString();
        if(x.includes('7')){
            check = true;
            console.log('Boom!');    
        }         
    }
    if(!check) console.log('there is no 7 in the array')
}
checkSeven(arrB);
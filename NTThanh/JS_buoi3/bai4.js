//4. Given an input string, reverse the string word by word, the first word will be the last, and so on.

function reverseWords(str){
    var x = str.split(' ');
    y = x.filter(x => x !== '');
    return y.reverse().join(' ');
}

console.log(reverseWords('hello   world!  '));

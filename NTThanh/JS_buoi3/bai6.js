const axios = require('axios');

function promise(){
    axios.get('https://api.github.com/users/ptit9x')
        .then(function(res){
            console.log(res);
        })
        .catch(function(error){
            console.log(error);
        });
}


async function async(){
    try {
        const res=await axios.get('https://api.github.com/users/ptit9x');
        console.log(res);
    } catch (error) {
        console.log(error);
    }
}
async();

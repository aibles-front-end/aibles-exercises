document.querySelector(".signup_link a").addEventListener("click", ()=>{
    document.querySelector("#signup").classList.add("active");
    document.querySelector("#login").classList.add("remove");
});

document.querySelector(".login_link a").addEventListener("click", ()=>{
    document.querySelector("#signup").classList.remove("active");
    document.querySelector("#login").classList.remove("remove");
});

var accounts = [
    {
        username: "thanh",
        password: "123456",
    },
    {
        username: "mai",
        password: "123456",
    },
    {
        username: "linh",
        password: "3123467",
    },
    {
        username: "anh",
        password: "456789",
    }
]

document.querySelector("#btnLogin").addEventListener("click",  function(){ 
    const username = document.querySelector("#loginForm .username").value;
    const password = document.querySelector("#loginForm .password").value;

    if ((username.trim() === "") || (password.trim() === "")) {
        alert("Username | Password password not be blank");
    }
    else {
        var check = false;
        for (var i = 0; i < accounts.length; i++) {
            if (accounts[i].username === username) {
                if (accounts[i].password === password) {
                    alert("Login successfully ");
                } else {
                    alert("Invalid username/password");
                    window.location.reload();
                }
                check = true;
                break;
            }
        }
        if (!check) {
            alert("Account not found");
            window.location.reload();
        }
    }
});

document.querySelector("#btnSignup").addEventListener("click",  function(){    
    const username = document.querySelector("#signupForm .username").value;
    const password = document.querySelector("#signupForm .password").value;
    const confimPass = document.querySelector(".confirmPass").value;

    var check = true;
    if ((username.trim() === "") || (password.trim() === "") || confimPass.trim() === "") {
        alert("Username | Password | Confirm password not be blank");
        check = false;
    }
    else {
        for (var i = 0; i < accounts.length; i++) {
            if (accounts[i].username === username) {
                alert("Account already exist");
                check = false;
                break;
            }
        }
        if(password !== confimPass){
            alert("Password not be match Confirm-password");
            check = false;
        }
        
    }
    if(check) {
        alert("Register successfull" );
        accounts.add({username, password});

    }
});





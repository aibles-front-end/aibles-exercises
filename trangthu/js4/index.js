axios.defaults.baseURL = 'https://60becf8e6035840017c17a48.mockapi.io/api';
let g_id;

function appendData(users){
    const tbody=document.getElementById("tbody");
    tbody.innerHTML="";
    users.map((user, index)=>tbody.innerHTML += (`
        <tr key=${index}>
            <td>${user.id}</td>
            <td>${user.name}</td>
            <td>${user.email}</td>
            <td>${user.city}</td>
            <td>
                <i class="fas fa-user-edit" onclick="displayEditUser(${index})"></i>
                <i class="delete fas fa-trash-alt" style="margin-left: 20px" onclick="deleteUser(${user.id})"></i>
            </td>
        </tr>
    `))
} 

function getData(){
    displayHomePage()
    axios.get('/users')
        .then((res)=>appendData(res.data))
        .catch((error)=>console.log(error))
}

getData()

const user=document.getElementsByClassName("form-control");

function createUser(){
    const name=user[0].value;
    const email=user[1].value;
    const city=user[2].value;
    if(!name.trim() || !email.trim() || !city.trim()){
        alert("Name | Email | City not be blank");
        return;
    }
    axios.post('/users', {
        name: name,
        email: email,
        city: city,
    }) 
        .then(()=>
            getData(),
            alert("User Created Successful") 
        )
        .catch((error)=>alert(error))
}

function editUser(){
    const name=user[3].value;
    const email=user[4].value;
    const city=user[5].value;
    if(!name.trim() || !email.trim() || !city.trim()){
        alert("Name | Email | City not be blank");
        return;
    }
    axios.put('/users/'+g_id, {
        name: name,
        email: email,
        city: city,
    })
        .then(()=>getData())
        .catch((error)=>alert(error))
}

function deleteUser(id){
    if(confirm("Do you really want to delete this user?")){
        axios.delete('/users/'+id)
            .then(()=>getData())
            .catch((error)=>alert(error))
    }
}

function displayHomePage(){
    document.getElementsByClassName('homepage')[0].style.display="block";
    document.getElementsByClassName('create-user')[0].style.display="none";
    document.getElementsByClassName('edit-user')[0].style.display="none";
}

function displayCreateUser(){
    document.getElementsByClassName('homepage')[0].style.display="none";
    document.getElementsByClassName('create-user')[0].style.display="block";
    document.getElementsByClassName('edit-user')[0].style.display="none";
}

function displayEditUser(index){
    const table = document.getElementById('table');
    const data=table.rows.item(index+1).cells;
    g_id=data.item(0).innerHTML;
    document.getElementById("edit-inputName").value = data.item(1).innerHTML;
    document.getElementById("edit-inputEmail").value = data.item(2).innerHTML;
    document.getElementById("edit-inputCity").value = data.item(3).innerHTML;

    document.getElementsByClassName('homepage')[0].style.display="none";
    document.getElementsByClassName('create-user')[0].style.display="none";
    document.getElementsByClassName('edit-user')[0].style.display="block";
}
import { Modal, notification } from "antd";
import { deleteUser, getUser } from "../../apis/User.api";
import "./ConfirmModal.scss";

const ConfirmModal = ({ del, setDel, setUser }) => {
  const confirmDelete = () => {
    deleteUser(del.id)
      .then(() => displayUser())
      .catch(() =>
        notification["error"]({
          message: "User Delete Failed",
          placement: "bottomLeft",
        })
      );
  };

  const displayUser = () => {
    setDel(null);
    getUser()
      .then((res) => setUser(res.data))
      .then(() => {
        notification["success"]({
          message: "User Delete Successfully",
          placement: "bottomLeft",
        });
      })
      .catch((error) => console.log(error));
  };

  return (
    <Modal
      visible={del}
      okText="OK"
      onOk={confirmDelete}
      onCancel={() => {
        setDel(null);
      }}
    >
      Do you want to delete this user?
    </Modal>
  );
};

export default ConfirmModal;

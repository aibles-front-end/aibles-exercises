import { useEffect, useState } from "react";
import {
  BiEdit,
  FaSortAmountDownAlt,
  MdDelete,
  RiArrowUpDownFill,
} from "react-icons/all";
import InputModal from "../InputModal/InputModal";
import ConfirmModal from "../ConfirmModal/ConfirmModal";
import { Button, Pagination } from "antd";
import "./MainContent.scss";
import { getUser } from "../../apis/User.api";

const MainContent = () => {
  const [user, setUser] = useState([]);
  const [create, setCreate] = useState(false);
  const [edit, setEdit] = useState(null);
  const [del, setDel] = useState(null);

  useEffect(() => {
    getUser()
      .then((res) => setUser(res.data))
      .catch((error) => console.log(error));
  }, []);

  function itemRender(current, type, originalElement) {
    if (type === "prev") {
      return <Button className="prev-button">Previous</Button>;
    }
    if (type === "next") {
      return <Button className="next-button">Next</Button>;
    }
    return originalElement;
  }

  return (
    <div className="main-content">
      <div className="main-content-header">
        <span className="title">Hover Data Table</span>
        <Button
          type="primary"
          className="btn"
          onClick={() => {
            setCreate(true);
          }}
        >
          CREATE USER
        </Button>
      </div>

      <div className="wrap-table">
        <table className="table">
          <thead>
            <tr>
              <th>
                <span className="th-title">No.</span>
                <span className="th-sort">
                  <FaSortAmountDownAlt />
                </span>
              </th>

              <th>
                <span className="th-title">Name</span>
                <span className="th-sort">
                  <RiArrowUpDownFill />
                </span>
              </th>

              <th>
                <span className="th-title">City</span>
                <span className="th-sort">
                  <RiArrowUpDownFill />
                </span>
              </th>

              <th>
                <span className="th-title">Email</span>
                <span className="th-sort">
                  <RiArrowUpDownFill />
                </span>
              </th>

              <th>
                <span className="th-title">Option</span>
                <span className="th-sort">
                  <RiArrowUpDownFill />
                </span>
              </th>
            </tr>
          </thead>

          <tbody>
            {user.map((item, index) => (
              <tr className="row" key={index}>
                <td>{index + 1}</td>
                <td>{item.name}</td>
                <td>{item.city}</td>
                <td>{item.email}</td>
                <td>
                  <span
                    className="option-icon"
                    onClick={() => {
                      setEdit(item);
                    }}
                  >
                    <BiEdit />
                  </span>
                  <span
                    className="delete option-icon"
                    onClick={() => {
                      setDel(item);
                    }}
                  >
                    <MdDelete />
                  </span>
                </td>
              </tr>
            ))}
          </tbody>

          <tfoot>
            <tr>
              <th>No.</th>
              <th>Name</th>
              <th>City</th>
              <th>Email</th>
              <th>Option</th>
            </tr>
          </tfoot>
        </table>
      </div>

      <div className="table-foot">
        <span className="table-infor">
          Showing 1 to {user.length} of {user.length} users
        </span>

        <Pagination
          defaultCurrent={1}
          total={60}
          itemRender={itemRender}
          showSizeChanger={false}
        />
      </div>

      <InputModal
        create={create}
        setCreate={setCreate}
        edit={edit}
        setEdit={setEdit}
        setUser={setUser}
      />

      <ConfirmModal del={del} setDel={setDel} setUser={setUser} />
    </div>
  );
};

export default MainContent;

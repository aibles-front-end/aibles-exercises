function formatMoney(num){
    if(num<0){
        return 0;
    }
    return new Intl.NumberFormat().format(num);
}

console.log(formatMoney(10000000000));
console.log(formatMoney(-5));
console.log(formatMoney(222444));
const axios = require('axios');

function getDataByAxios(){
    axios.get('https://api.github.com/users/ptit9x')
        .then(function(res){
            console.log(res);
        })
        .catch(function(error){
            console.log(error);
        });
}

// getDataByAxios();

async function getDataByAA(){
    try {
        const res=await axios.get('https://api.github.com/users/ptit9x');
        console.log(res);
    } catch (error) {
        console.log(error);
    }
}

getDataByAA();
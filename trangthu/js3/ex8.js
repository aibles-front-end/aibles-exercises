function isPandigital(num){
    let str=BigInt(num).toString();
    for(let i=0; i<10; i++){
        if(!str.includes(i)){
            return false;
        }
    }
    return true;
}


console.log(isPandigital(1234));
console.log(isPandigital(1234567890));
console.log(isPandigital(112233445566778899));



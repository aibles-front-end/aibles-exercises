//Ex1:

const objects = [
  {
    number: 45,
  },
  {
    number: 4,
  },
  {
    number: 9,
  },
  {
    number: 16,
  },
  {
    number: 25,
  },
  {
    number: 16,
  },
  {
    number: 24,
  }
];

const tmpArr=objects.map(x=>x.number);
const resArr=tmpArr.filter(x=>x>20);
console.log(resArr);

const totalNum=resArr.reduce((accumulator, currentValue)=>accumulator+currentValue);
console.log(totalNum);

const { default: axios } = require("axios");

const URL1='https://api.github.com/users/ptit9x';
const URL2='https://api.github.com/users/google';

const promise1=axios.get(URL1);
const promise2=axios.get(URL2);

Promise.all([promise1, promise2])
    .then(function(res){
        console.log(res);
});
axios.defaults.baseURL = ' https://60becf8e6035840017c17a48.mockapi.io/api';
var table = document.getElementById("table");
function getData() {
    axios.get('/users')
        .then(function(response) {
            appendData(response.data)
        })
        .catch(function(error) {
            console.log(error);
        })
}
getData();
function appendData(users) {
    const tbody = document.getElementById("tbody");
    tbody.innerHTML = "";
    users.map((user, index)=>tbody.innerHTML += (`
        <tr key=${index}>
            <td>${user.id}</td>
            <td>${user.name}</td>
            <td>${user.email}</td>
            <td>${user.city}</td>
            <td>
                <i class="fas fa-user-edit" onclick="displayEditUser(${index})"></i>
                <i class="fa fa-trash" onclick="deleteUser(${user.id})"></i>
            </td>
        </tr>
    `))
}
const user = document.getElementsByClassName("form-control")
function createUser() {
    const name = user[0].value;
    const email = user[1].value;
    const city = user[2].value;
    if(name.length == 0|| email.length == 0|| city.length == 0) {
        alert("You must complete the input.")
    }
    else {
        axios.post('/users',{
            name: name,
            email: email,
            city: city,
        })
        .then(function(response) {
            getData()
            alert("Successful")
        })
        .catch(function(error){
            alert(error)
        })
    }
}
function deleteUser(id) {
    if(confirm("Do you really want to delete this user?")) {
        axios.delete('/users/' + id)
        .then(function(response){
            getData()
        })
        .catch(function(error){
            alert(error)
        })
    }
}
function editUser() {
    const name = user[3].value;
    const email = user[4].value;
    const city = user[5].value;
    if(name.length == 0|| email.length == 0|| city.length == 0) {
        alert("You must complete the input.")
    }
    else {
        axios.put('/users/'+editId,{
            name: name,
            email: email,
            city: city,
        })
        .then(function(response) {
            getData()
            alert("Successful")
        })
        .catch(function(error){
            alert(error)
        })
    }
}
function displayEditUser(index){
    const tableEdit = table.rows.item(index+1).cells;
    editId = tableEdit.item(0).innerHTML;
    document.getElementById("editName").value = tableEdit.item(1).innerHTML;
    document.getElementById("editEmail").value = tableEdit.item(2).innerHTML;
    document.getElementById("editCity").value = tableEdit.item(3).innerHTML;
}
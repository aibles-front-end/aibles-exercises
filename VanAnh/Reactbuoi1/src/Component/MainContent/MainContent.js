import { useState, useEffect } from "react"
import { deleteUser, getUser } from "../../apis/api"
import ModalUser from "./ModalUser"
import { FaEdit, FaTrash } from "react-icons/fa"
import { notification, Popconfirm, Button } from "antd"
import "./MainContent.scss"


const MainContent = () => {
    const [users, setUsers] = useState([])
    const [editModal, setEditModal] = useState(null)
    const [isDelete, setIsDelete] = useState(null)

  useEffect(() => {
    const fetchData = async () => {
      getUser()
        .then((response) => {
          setUsers(response.data)
          console.log(response.data)
        })
        .catch((error) => console.log(error))
    }
    fetchData()
  }, [])

  const onConfirm = () => {
    deleteUser(isDelete)
      .then(() => displayData())
      .catch(() => {
        notification["error"]({
          message: "Delete user failed",
        })
      })
  }

  const displayData = () => {
    getUser()
      .then((response) => {
        setUsers(response.data)
        notification["success"]({
          message: "Delete user successful",
        })
      })
      .catch((error) => console.log(error))
  }    
    return(
        <div className="maincontent">
            <p>Hover Data Table</p>
            <ModalUser
                editModal={editModal}
                setEditModal={setEditModal}
            ></ModalUser>
           <div className="container">
           <table className="table" id="table">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">City</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="tbody">
                    {
                        users.map((user, index) => (
                          <tr key={index}>
                          <td>{index + 1}</td>
                          <td>{user.name}</td>
                          <td>{user.email}</td>
                          <td>{user.city}</td>
                          <td className="group-btn">
                            <Button
                              style={{ marginRight: 10 }}
                              onClick={() => {
                                setEditModal(user)
                              }}
                            >
                              <FaEdit style={{ marginRight: 5 }} />
                              Edit
                            </Button>
                            <Popconfirm
                              title="Are you sure to delete this user?"
                              onConfirm={onConfirm}
                              okText="Yes"
                              cancelText="No"
                            >
                              <Button onClick={() => setIsDelete(user.id)}>
                                <FaTrash style={{ marginRight: 5 }} />
                                Delete
                              </Button>
                            </Popconfirm>
                          </td>
                        </tr> 
                        ))
                    }
                </tbody>
                <tfoot>
                    <tr>
                        <td>ID</td>
                        <td>Name</td>
                        <td>Email</td>
                        <td>City</td>
                    </tr>
                </tfoot>
            </table>
           </div>
            <div className="end">
                <div className="text">
                    Showing 1 to {users.length} of {users.length} entries
                </div>
                <div className="previous row">
                    <div className="col element">
                        <a href="#" className="text-white">
                            <span className="text-secondary text-previous">Previous</span>
                        </a>
                    </div>
                    <div className="col element bg-primary">
                        <a href="#" className="text-primary">
                            <span className="text-white">1</span>
                        </a>
                    </div>
                    <div  className="col element">
                        <a href="#" className="text-white">
                            <span className="text-secondary">2</span>
                        </a>
                    </div>
                    <div  className="col element">
                        <a href="#" className="text-white">
                            <span className="text-secondary">3</span>
                        </a>
                    </div>
                    <div  className="col element">
                        <a href="#" className="text-white">
                            <span className="text-secondary">4</span>
                        </a>
                    </div>
                    <div  className="col element">
                        <a href="#" className="text-white">
                            <span className="text-secondary">5</span>
                        </a>
                    </div>
                    <div  className="col element">
                        <a href="#" className="text-white">
                            <span className="text-secondary">6</span>
                        </a>
                    </div>
                    <div  className="col element">
                        <a href="#" className="text-white">
                            <span className="text-secondary text-next">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default MainContent
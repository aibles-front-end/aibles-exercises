import { useState } from "react"
import { getUser, postUser, putUser } from "../../apis/api"
import { Form, Input, Button, Modal, notification } from "antd"
import "antd/dist/antd.css"

const ModalUser = (props) => {
  const [name, setName] = useState("")
  const [email, setEmail] = useState("")
  const [city, setCity] = useState("")
  const [users, setUsers] = useState([])
  const [addModal, setAddModal] = useState(false)

  const onFinish = () => {
    if (addModal) {
      setAddModal(false)
      let newUser = { name, email, city }
      postUser(newUser)
        .then(() => displayData())
        .catch(() => {
          notification["error"]({
            message: "Add user failed"
          })
        })
    }
    if (props.editModal) {
      props.setEditModal(null)
      let newUser = { name, email, city }
      putUser(newUser, props.editModal.id)
        .then(() => displayData())
        .catch(() => {
          notification["error"]({
            message: "Edit user failed"
          })
        })
    }
  }

  const displayData = () => {
    getUser()
      .then((response) => {
        setUsers(response.data)
        notification["success"]({
          message: addModal ? "Add user successful" : "Edit user successful"
        })
        setInterval(() => {
          window.location.reload()
        }, 1000)
      })
      .catch((error) => console.log(error))
  }

  const handleCancel = () => {
    setAddModal(false)
    props.setEditModal(null)
  }

  return (
    <>
      <Button
        type="primary"
        style={{ marginBottom: 10, fontSize: 10 }}
        onClick={() => setAddModal(true)}
      >
        Add
      </Button>
      <Modal
        title={addModal ? "Add user" : "Edit user"}
        visible={addModal || props.editModal}
        onCancel={handleCancel}
        footer={""}
      >
        <Form
          name="basic"
          labelCol={{ span: 5 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinish}
        >
          <Form.Item
            label="Name"
            name="name"
            onChange={(evt) => setName(evt.target.value)}
            rules={[{ required: true, message: "Please input your username!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Email"
            name="email"
            onChange={(evt) => setEmail(evt.target.value)}
            rules={[{ required: true, message: "Please input your email!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="City"
            name="city"
            onChange={(evt) => setCity(evt.target.value)}
            rules={[{ required: true, message: "Please input your city!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit" className="btn-submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  )
}

export default ModalUser

import { FaAngleRight } from "react-icons/fa"
import  './HeaderContent.scss'

function HeaderContent() {
    return(
        <div className="headercontent">
            <div className="header-content">
                <div className="text-begin-table">
                    <h1>
                        DATA TABLES
                        <small>advanced table</small>
                    </h1>
                </div>
                <div className="headercontent-end">
                    <a href="#" className="choose">
                        <i className="fa fa-clock left-sidebar text-dark" aria-hidden="true"></i>
                        <span className="text-dark mx-2">Home</span>
                    </a>
                    <span  className="mx-2 text-black-50"><FaAngleRight/></span>
                    <a href="#" className="choose">
                        <span className="text-dark mx-2">Tables</span>
                    </a>
                    <span  className="mx-2 text-black-50"><FaAngleRight/></span>
                    <a href="#" className="choose">
                        <span className="text-dark mx-2">Data tables</span>
                    </a>
                </div>
            </div>
        </div>
    );
}
export default HeaderContent
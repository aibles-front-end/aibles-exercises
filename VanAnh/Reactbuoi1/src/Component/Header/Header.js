import { FaBars, FaEnvelope, FaBell, FaFlag, FaCogs } from "react-icons/fa"
import USER from '../../assets/img/user.jpg'
import  './Header.scss'

function Header() {
    return (
        <div className="header">
            <div className="text-begin">
                <b>AdminLTE</b>
            </div>
            <div className="header-end">
                <a href="#" className=" sidebar-toggle text-white " role="button">
                    <span><FaBars/></span>
                </a>
                <div className="mess-noti-flag ">
                    <a href="#" className="item" role="button">
                        <span  className="icon"><FaEnvelope/></span>
                        <span className="badge number bg-success text-white">4</span>
                    </a>
                    <a href="#" className="item">
                        <span  className="icon"><FaBell/></span>
                        <span className="badge number bg-warning text-white">10</span>
                    </a>
                    <a href="#" className="item ">
                        <span  className="icon"><FaFlag/></span>
                        <span className="badge number bg-danger text-white">9</span>
                    </a>
                </div>
                <a href="#" className="profile">
                    <img src={USER}></img>
                    <p className="text-light text">Alexander Pierce</p>
                </a>
                <a href="#" className="icon-end">
                    <span  className="text-light"><FaCogs/></span>
                </a>
            </div>
        </div>
    );
}


export default Header
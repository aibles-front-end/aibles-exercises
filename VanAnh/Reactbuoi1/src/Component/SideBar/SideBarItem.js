const SidebarItem = (props) => {
    return (
      <>
        <li className="element">
          <div className="choose">
            <span className="left-sidebar">{props.leftIcon}</span>
            <span className="text">{props.text}</span>
          </div>
          <div className="right-icon">{props.rightIcon}</div>
        </li>
      </>
    )
  }
  
  export default SidebarItem
  
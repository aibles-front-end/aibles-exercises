import React from "react"
import {FaAngleLeft ,FaTachometerAlt, FaFile, FaTh, FaChartPie, FaLaptop, FaEdit, 
    FaTable, FaCalendar, FaEnvelope, FaFolder, FaShare, FaBook, FaCircle, FaSearch
} from 'react-icons/fa'
import SidebarItem from "./SideBarItem";
import SidebarLabel from "./SideBarLable";
import USER from '../../assets/img/user.jpg';
import  './SideBar.scss'

function SideBar() {
    return (
        <div className="main-sidebar">
            <div className="user">
                <img src={USER}></img>
                <div className="infor">
                    <p>Alexander Pierce</p>
                    <div className="online">
                        <span className="green-color"><FaCircle/></span>
                        <p>Online</p>
                    </div>
                </div>
            </div>
            <div className="search">
                <input type="text" className="form-control" placeholder="Search..."></input>
                <span className="input-group-btn">
                    <button type="submit" className="btn btn-flat">
                        <span className="icon-search"><FaSearch/></span>
                    </button>
                </span>
            </div>
            <div className="sidebar-menu">
                <li className="header">MAIN NAVIGATION</li>
                <div className="nav">
                        <SidebarItem
                            leftIcon={<FaTachometerAlt />}
                            text="Dashbroad"
                            rightIcon={<FaAngleLeft />}
                        ></SidebarItem>
                
                        <SidebarItem
                            leftIcon={<FaFile />}
                            text="Layout Options"
                            rightIcon={<span className=" badge right item text-light bg-primary">4</span>}
                        ></SidebarItem>
                   
                        <div>
                        <SidebarItem
                            leftIcon={<FaTh />}
                            text="Widgets"
                            rightIcon={<span className=" badge right text-light bg-success">new</span>}
                        ></SidebarItem>
                        
                        </div>
                
                        <SidebarItem
                            leftIcon={<FaChartPie />}
                            text="Charts"
                            rightIcon={<FaAngleLeft />}
                        ></SidebarItem>

                        <SidebarItem
                            leftIcon={<FaLaptop />}
                            text="UI Elements"
                            rightIcon={<FaAngleLeft />}
                        ></SidebarItem>

                        <SidebarItem
                            leftIcon={<FaEdit />}
                            text="Forms"
                            rightIcon={<FaAngleLeft />}
                        ></SidebarItem>
                
                        <SidebarItem
                            leftIcon={<FaTable />}
                            text="Tables"
                            rightIcon={<FaAngleLeft />}
                        ></SidebarItem>
            
                        <SidebarItem
                            leftIcon={<FaCalendar />}
                            text="Calendar"
                            rightIcon={
                                <span className=" badge item  text-light bg-danger">3</span>
                            }
                        ></SidebarItem>
                
                        <SidebarItem
                            leftIcon={<FaEnvelope />}
                            text="Mailbox"
                            rightIcon={
                                <span className=" badge item text-light bg-warning">12</span>
                            }
                        ></SidebarItem>
                   
                        <SidebarItem
                            leftIcon={<FaFolder />}
                            text="Examples"
                            rightIcon={<FaAngleLeft />}
                        ></SidebarItem>
                   
                        <SidebarItem
                            leftIcon={<FaShare/>}
                            text="Multilevel"
                            rightIcon={<FaAngleLeft />}
                        ></SidebarItem>
                   
                        <SidebarItem
                            leftIcon={<FaBook />}
                            text="Documentation"
                            rightIcon={<FaAngleLeft />}
                        ></SidebarItem>
                </div>
                <div className="header-lable"><p>LABLES</p></div>
                <div className="element">
                    <SidebarLabel  text="Important"></SidebarLabel>
                </div>
                <div className="element">
                    <SidebarLabel labelColor="red" text="Warning"></SidebarLabel>
                </div>
                <div className="element">
                    <SidebarLabel labelColor="yellow" text="Information"></SidebarLabel>
                </div>
            </div>
        </div>
    );
}


export default SideBar
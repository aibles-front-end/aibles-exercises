import * as FaIcons from "react-icons/fa"

const SidebarLabel = (props) => {
  return (
    <ul className="lable">
      <div className="lable-item-left">
        <span className="item-left-icon item-left-icon-danger">
          <FaIcons.FaRegCircle />
        </span>
        <span className="content">{props.text}</span>
      </div>
    </ul>
  )
}

export default SidebarLabel


import axios from "axios"

axios.defaults.baseURL = "https://60becf8e6035840017c17a48.mockapi.io/api"

export function getUser(data) {
    return axios.get("/users")
}
export function deleteUser(id) {
    return axios.delete(`/users/${id}`)
}  
export function postUser(value) {
    return axios.post("/users", {
      name: value.name,
      email: value.email,
      city: value.city
    })
}
  
export function putUser(value, id) {
    return axios.put(`/users/${id}`, {
      name: value.name,
      email: value.email,
      city: value.city
    })
}
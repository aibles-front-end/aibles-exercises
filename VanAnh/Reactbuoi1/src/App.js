import './App.css'
import React, { Component } from 'react'
import Header from "./Component/Header/Header"
import SideBar from './Component/SideBar/SideBar'
import HeaderContent from "./Component/HeaderContent/HeaderContent";
import MainContent from "./Component/MainContent/MainContent";
import "./assets/scss/index.scss"

function App() {
  return (
    <div className="index">
      <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
      <Header/>
      <div className="d-flex">
        <SideBar/>
        <div className="content">
            <HeaderContent />
            <MainContent />
        </div>
      </div>
    </div>
  )
}

export default App;

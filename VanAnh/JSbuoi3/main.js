//exercise 1
//Using .filter and .map to get new array have number > 20 and format numbers to array number
//Using .reduce get total number in objects array
const objects = [
    {
        number: 45,
    },
    {
        number: 4,
    },
    {
        number: 9,
    },
    {
        number: 16,
    },
    {
        number: 25,
    },
    {
        number: 16,
    },
    {
        number: 24,
    }
    
];
const arr1 = objects.map (x => x.number);
const result = arr1.filter (number => number > 20);
console.log(result);
const total = arr1.reduce (function(acc,cur) {
   return acc + cur;
})
console.log(total);

//exercise2
//write a function formatMoney(amount) to format currency money
function  formatMoney(amount) {
    if(amount <= 0) return "0";
    return amount.toLocaleString("en-CA");
}
console.log(formatMoney(100000000));

//exercise3
//"Boom!" if the number 7 appears in the array
function sevenBoom(arr) {
    if(arr.join().includes(7)) {
        return "Boom!";
    }
    return "there is no 7 in the array";
}

console.log(sevenBoom([2, 4, 5, 97]));

//exercise4
// Given an input string, reverse the string word by word, the first word will be the last, and so on
function reverseWords (str) {
    let str1 = str.split(" ");
    let newArr = [];
    for(let word of str1) {
        if (word !== "") newArr.push(word);
    }
    newArr.reverse();
    return newArr.join(" ");
}
console.log(reverseWords("    the     sky is    blue "));

//exercise5
// know the total number of non-nested items in the nested array
function getLength(arr) {
   if(arr != "") {
    let count = 0;
    for (let x of arr) {
        if(Array.isArray(x)) count += getLength(x);
        else count++;
    }
    return count;
   }
   else return 0;
}
console.log(getLength([1,[2,[4,[2,9]]]]));

//exercise6
const axios = require('axios');
function callAPIbyPromise() {
    axios.get('https://api.github.com/users/ptit9x')
    .then(function (response) {
        console.log(response.data);
    })
    .catch(function (error) {
        console.log(error);
    })
}
callAPIbyPromise();
async function callAPIbyAsyncAwait() {
    try { 
        const response = await axios.get('https://api.github.com/users/ptit9x')
        console.log(response.data);
    }
    catch(error) {
        console.log(error);
    }
}
callAPIbyAsyncAwait();

//exercise7
const axios = require('axios')
function getAPIPtit9x() {
    return axios.get('https://api.github.com/users/ptit9x')
}
function getAPIgoogle() {
    return axios.get('https://api.github.com/users/google')    
}
Promise.all([getAPIPtit9x(), getAPIgoogle()]) 
    .then(function (response) {
        console.log(response);
    })
    .catch(function (error) {
        console.log(error);
    })

//exercise8
//Write a function that takes an integer, returning true if the integer is pandigital, and false otherwise.
function isPandigital(number) {
    var num = number.toString();
    for (let i = 0; i < 10; i++) {
        let count = 0;
        for (let x of num){
            if(i == x) count = 1;
        }
        if(count == 0) return false;
    }
    return true;
}
console.log(isPandigital(823130931364657));

//exercise9
//Create a function which returns how many Friday 13ths there are in a given year.

function howUnlucky(year) {
    let month = 1;
    let count = 0;
    while (month < 12) {
        let d = new Date(year, month, 13);
        if(d.getDay() == 5) count++;
        month++;
    }
    return count;
}
console.log(howUnlucky(2026));
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { FastField, Form, Formik } from 'formik';
import * as yup from 'yup';

import InputField from '../../commons/custom-filed/inputFiled';
import './userForm.style.scss';

UserForm.propTypes = {
    onSubmit: PropTypes.func,
}

function UserForm(props) {
    const{initialValues, isAddMode, handleCancel} = props;    
    const emptyFieldErr = 'Please enter this field!';
    const handleOutModal = () => {
        handleCancel();
    }

    const validationSchema = yup.object().shape({
        name: yup.string().required(emptyFieldErr),
        avatar: yup.string().required(emptyFieldErr),
        email: yup.string().required(emptyFieldErr),
        city: yup.string().required(emptyFieldErr),
        image: yup.string().required(emptyFieldErr),
    });
    return (
        <Formik 
            initialValues = {initialValues}
            validationSchema = {validationSchema}
            onSubmit={ (values,{resetForm}) => {
                resetForm();
                props.onSubmit(values);
            }}
            enableReinitialize={true}
        >
            { 
                formikProps => {
                    const {values, errors, touched} = formikProps;
                    return (
                        <Form className = 'todo__form'>
                            <FastField 
                                name = 'name'
                                component = {InputField}

                                label = 'Name'
                                placeholder = 'Enter your name'
                            />

                            <FastField 
                                name = 'avatar'
                                component = {InputField}

                                label = 'Avatar'
                                placeholder = 'Enter your avatar'
                            />

                            <FastField 
                                name = 'email'
                                component = {InputField}

                                label = 'Email'
                                placeholder = 'Enter your email'
                            />

                            <FastField 
                                name = 'city'
                                component = {InputField}

                                label = 'City'
                                placeholder = 'Enter your city'
                            /> 

                            <FastField 
                                name = 'image'
                                component = {InputField}

                                label = 'Image'
                                placeholder = 'Enter your image'
                            />  
                            <button
                                className= {isAddMode ? 'btn btn-primary' : 'btn btn-success'}
                                type='submit'
                            >{isAddMode ? 'Add User' : 'Save'}</button>
                            <button 
                                type = 'reset'
                                className='btn btn-danger btn-cancel'
                                onClick={handleOutModal}
                            > Cancel</button>
                            
                        </Form>
                    );
                }
            }
        </Formik>
        
    );
}

export default UserForm;
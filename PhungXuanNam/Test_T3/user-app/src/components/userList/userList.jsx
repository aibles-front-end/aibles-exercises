import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope, faCity, faPen, faTrash } from '@fortawesome/free-solid-svg-icons';
import { Modal, Button } from 'antd';
import './userList.style.scss';

function UserList(props) {
    const {users , handleDeleteUser, openModal} = props;  
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [userId, setUserId] = useState(-1);
    const imgDefault ='https://tilaso.vn/assets/admin/images/default-image.jpg';

    const showModal = (id) => {
        setIsModalVisible(true);
        setUserId(id);
    };

    const handleDelete = () => {
        handleDeleteUser(userId);
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };
    return (
        <div className='user-list'>
            
            <div className='row'>
                {users.map((user, index) => {
                    return (
                        <div className='col-xl-3 col-lg-4 col-md-6 col-sm-12' key={index}>
                            <div className='user-item'>
                                <div className='user-item__box'>
                                    <img
                                        src={user.avatar.trim() ? user.avatar: imgDefault}
                                        className='avt'
                                    />
                                    <div className='user-item__info'>
                                        <div className='user-item__info--name'>
                                            {user.name}
                                        </div>
                                        <div className='user-item__info--address'>
                                            <span className='email d-flex'>
                                                <FontAwesomeIcon icon={faEnvelope} className='icon'/>
                                                {user.email}
                                            </span>
                                            <span className='city d-flex'>
                                            <FontAwesomeIcon icon={faCity} className='icon'/>
                                                {user.city}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <img
                                    src={user.image.trim() ? user.image : imgDefault}
                                    className='img-post'
                                />

                                <div className='control'>
                                    <button
                                        onClick= {() =>  openModal(user.id)}
                                        className='btn btn-warning'
                                    >
                                        <FontAwesomeIcon icon={faPen} className = 'btn-icon'/>
                                        Edit User
                                    </button>
                                    <button className='btn btn-danger ml-4' onClick={() => showModal(user.id)}>
                                        <FontAwesomeIcon icon={faTrash} className = 'btn-icon'/>
                                        Delete User
                                    </button>
                                </div>
                            </div>
                            
                        </div>
                    );
                })}
            </div>
            <Modal visible={isModalVisible}  onCancel={handleCancel} footer = {null}>
                <p>Do you want to delete user?</p>
                <div className="confirm-delete">
                    <button className="btn btn-primary" onClick={handleCancel}>Cancel</button> &emsp;
                    <button className="btn btn-danger" onClick={handleDelete}>OK</button>
                </div>
            </Modal>
        </div>
    );
}

export default UserList;

import usersApi from '../apis/usersApi';
import * as types from './types';

export const getAll = () => async (dispatch) => {
    try {
        const response = await usersApi.getAll();
        dispatch({
            type: types.GET_ALL,
            payload: response,
        });
    } catch (error) {
        Promise.reject(error);
    }
};

export const createUser = (data) => async (dispatch) => {
    try {
        const response = await usersApi.create(data);
        dispatch({
            type: types.CREATE_USER,
            payload: response,
        });
        return Promise.resolve(response);
    } catch (error) {
        return Promise.reject(error);
    }
};

export const deleteUser = (id) => async (dispatch) => {
    try {
        await usersApi.remove(id);
        dispatch({
            type: types.DELETE_USER,
            payload: id,
        });
    } catch (error) {
        Promise.reject(error);
    }
};

export const editUser = (user) => async (dispatch) => {
    const id = user.id;
    try {
        await usersApi.update(id, user);

        dispatch({
            type: types.UPDATE_USER,
            payload: user,
        });
    } catch (error) {
        Promise.reject(error);
    }
};

import axiosClient from '../utilities/axiosClient';
const usersApi = {
    getAll: () => {
        return axiosClient.get('/users');
    },

    getById: (id) => {
        const url = `/users/${id}`;
        return axiosClient.get(url);
    },

    create: (data) => {
        return axiosClient.post(`/users`, data);
    },

    update: (id, data) => {
        return axiosClient.put(`/users/${id}`, data);
    },

    remove: (id) => {
        return axiosClient.delete(`/users/${id}`);
    },
};

export default usersApi;

import React from 'react';

import Home from './pages/Home/Home';

import 'antd/dist/antd.css';
import './App.scss';

function App() {
    return (
        <Home/>
    );
}

export default App;

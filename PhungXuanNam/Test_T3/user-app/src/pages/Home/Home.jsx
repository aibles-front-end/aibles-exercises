import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Modal } from 'antd';

import { faPlus } from '@fortawesome/free-solid-svg-icons';
import UserList from '../../components/userList/userList';
import UserForm from '../../components/userForm/userForm';
import {ToastContainer } from 'react-toastify';

import * as actions from '../../actions/userAction';
import * as showToast from '../../commons/toastify/showToat';

import 'react-toastify/dist/ReactToastify.css';
import './home.style.scss';



function Home(props) {
    const dispatch = useDispatch();
    const users = useSelector((state) => state.users);
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [isAddMode, setIsAddMode] = useState(true);
    const [userId, setUserId] = useState();

    // Open --> AddEdit pmodal
    const openModal = (id) => {
        if(typeof id === 'string'){
            setIsAddMode(false);
        }
        else{
            setIsAddMode(true);
        }
        setUserId(id);
        setIsModalVisible(true);
    }
    
    
    const userEdited = useSelector(state =>{
        return state.users.find(user => user.id === userId);
    });

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const initialValues =  isAddMode ? 
        {
            name: '',
            avatar: 'http://t0.gstatic.com/licensed-image?q=tbn:ANd9GcRQnEA06hqnnZ4NEO8ffoik5d7jeJ0iAe50FDKh4OJmqzMgR-WoPDTzh39MJ5zm',
            email: '',
            city : '',
            image: 'https://i2-prod.manchestereveningnews.co.uk/incoming/article20575802.ece/ALTERNATES/s1200c/0_GettyImages-1232825729.jpg'

        } : userEdited;

    // add & edit
    const onSubmit = (data) => {
        if(isAddMode) {
            dispatch(actions.createUser({
                ...data
            }));
            setIsModalVisible(false);
            showToast.success('Create user successfully!!!')
        }
        else {
            dispatch(actions.editUser(data));
            showToast.success('Update user successfully!!!')
        }
        setIsModalVisible(false);
    }

    // GET ALL
    useEffect(() => {
        dispatch(actions.getAll());
    }, []);
    
    //  DELETE_USER
    const handleDeleteUser = (id) => {
            dispatch(actions.deleteUser(id));
            showToast.success('Delete user success')
    }
    return (
        <div className='content'>
            <h1 className='text-center'>User List Aibles</h1>
            <button 
                className='btn btn-primary add-user'
                onClick = {openModal}
            >
                <FontAwesomeIcon icon={faPlus} className='add-user__icon' />
                Add User
            </button>

            <UserList 
                users={users} 
                handleDeleteUser = {handleDeleteUser}
                openModal = {openModal}
            />

            <Modal 
                title= {isAddMode ? 'Add User' : 'Edit User'} 
                visible={isModalVisible}  
                footer = {null}
            >
                <UserForm
                    isAddMode = { isAddMode}
                    initialValues = { initialValues }
                    onSubmit = {onSubmit}
                    handleCancel = {handleCancel}
                />
            </Modal>
            <ToastContainer />
        </div>
    );
}

export default Home;

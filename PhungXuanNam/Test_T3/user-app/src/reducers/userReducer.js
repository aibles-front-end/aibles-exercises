import * as types from '../actions/types';
import {findIndex} from 'lodash';
const initialState = [];

function userReducer(state = initialState, action) {
    switch (action.type) {
        case types.GET_ALL:
            return action.payload;
        case types.CREATE_USER:
            return [
                ...state,
                action.payload
            ];
        case types.DELETE_USER:
            const index = findIndex(state, user =>{
                return user.id === action.payload;
            });
            const newState = [...state]
            newState.splice(index, 1);
            return [...newState];
        case types.UPDATE_USER:
            const idx = findIndex(state, user =>{
                return user.id === action.payload.id;
            })

            const stateEdited = [...state];
            stateEdited[idx] = action.payload;
            return stateEdited;
        default:
            return state;
    }
}

export default userReducer;

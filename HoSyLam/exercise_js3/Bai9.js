function howUnlucky(year) {
    let count = 0;
    for (let month = 0; month < 12; month++) {
        let date = new Date(year, month, 13);
        if (date.getDay() === 5) {
            count++;
        }
    }
    return count;
}
// Duyệt theo từng tháng, kiểm tra nếu ngày 13 của mỗi tháng của năm đó
// là thứ 6 thì tăng count

console.log(howUnlucky(2020));// 2
console.log(howUnlucky(2026));//  3
console.log(howUnlucky(2016));//  1

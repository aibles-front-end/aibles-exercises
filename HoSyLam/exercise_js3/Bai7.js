const axios = require('axios');

let request1 = axios.get(`https://api.github.com/users/ptit9x`),
    request2 = axios.get(`https://api.github.com/users/google`);


Promise.all([request1, request2])
    .then(responses => {
        responses.forEach((response) => {
            console.log(`Login name: ${response.data.login}, public repos: ${response.data.public_repos}`);
        });
    })
    .catch(err => { console.log(err); })
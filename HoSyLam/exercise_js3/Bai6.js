const axios = require('axios');

// First way using async await
(async () => {
    try {
        const response = await axios.get("https://api.github.com/users/ptit9x");
        console.log(response.data);
    } catch (err) { console.log("An error has occurred, pls try again."); }
})();

// Second way using Promise
axios.get("https://api.github.com/users/ptit9x")
    .then((response) => {
        console.log(response.data);
    })
    .catch(() => { console.log("An error has occurred, pls try again."); });

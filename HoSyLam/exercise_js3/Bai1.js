const objects = [
    {
        number: 45,
    },
    {
        number: 4,
    },
    {
        number: 9,
    },
    {
        number: 16,
    },
    {
        number: 25,
    },
    {
        number: 16,
    },
    {
        number: 24,
    }
];

// Using .filter and .map to get new array have number > 20
const mappedArr = objects.map(obj => obj.number);
const filteredArr = mappedArr.filter(num => num > 20);

// format numbers to array number. Expect: [4, 9, 16, 16]
const arrNumber = mappedArr.filter(num => num < 20);

// Using .reduce get total number in objects array
const reducer = (accumulator, currentValue) => accumulator + currentValue;

console.log(filteredArr);
console.log(arrNumber);
console.log(filteredArr.reduce(reducer));
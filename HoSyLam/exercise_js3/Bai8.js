function isPandigital(num) {
    num = String(BigInt(num));
    num = num.split("");

    const set = new Set(num);

    return set.size === 10 ? true : false;
}

console.log(isPandigital(98140723568910));// true
console.log(isPandigital(90864523148909));// false
// 7 is missing.
console.log(isPandigital(112233445566778899));// false
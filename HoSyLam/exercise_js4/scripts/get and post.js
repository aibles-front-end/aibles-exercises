axios.defaults.baseURL = 'https://60becf8e6035840017c17a48.mockapi.io/api';

(async () => {
    try {
        const response = await axios.get("/users");
        appendData(response.data);
    }
    catch (err) { alert(err); }
})();

const appendData = users => {
    const tbody = document.getElementById("tbody");

    users.map((user, index) => tbody.innerHTML += (`
        <tr key=${index}>
            <th scope="row">${user.id}</th>
            <td>${user.name}</td>
            <td>${user.email}</td>
            <td>${user.city}</td>
        </tr>
        `
    ));
}

const addDataBtn = document.querySelector('.add-btn')
    .addEventListener('click', async () => {
        const user = document.getElementsByClassName("form-control");

        const name = user[0].value,
            email = user[1].value,
            city = user[2].value

        if (!name || !email || !city) {
            alert("Name | Email | City must not be blank")
        }
        else {
            try {
                await axios.post('/users', {
                    name: name,
                    email: email,
                    city: city,
                })
                alert("User added!");
                window.location.reload();
            }
            catch (err) { alert(err); }
        }
    });

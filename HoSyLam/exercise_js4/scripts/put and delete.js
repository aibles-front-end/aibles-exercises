axios.defaults.baseURL = 'https://60becf8e6035840017c17a48.mockapi.io/api';
// PUT and DELETE
document.querySelector('.search-btn')
    .addEventListener('click', () => {
        const id = document.getElementById('id-input').value;
        if (!id) {
            alert('Pls enter an ID to search');
        } else if (id < 1) {
            alert('ID is invalid');
        }
        else {
            modifyUI(id);
        }
    });

const modifyUI = async (id) => {
    try {
        const user = await axios.get(`/users/${id}`);
        const foundUser = document.getElementById("found-user");

        foundUser.innerHTML = (`
        <h3>Found user</h3>
        <ul>
            <li>Name: ${user.data.name}</li>
            <li>Email: ${user.data.email}</li>
            <li>City: ${user.data.city}</li>
        </ul>

        <form class="d-flex">
            <div class="me-2">
                <input type="text" id="name" class="form-control input" placeholder="New name">
            </div>
            <div class="me-2">
                <input type="email" id="email" class="form-control input" placeholder="New email">
            </div>
            <div class="me-2">
                <input type="text" id="city" class="form-control input" placeholder="New city">
            </div>
        </form>
        `
        );

        const modifyBtn = document.querySelector('.modify-btn');
        modifyBtn.style.display = "block";
        modifyBtn.addEventListener('click', async () => {
            const newUser = document.getElementsByClassName("input");
            let newName = newUser[0].value,
                newEmail = newUser[1].value,
                newCity = newUser[2].value;

            if (!newName) {
                newName = user.data.name;
            }
            if (!newEmail) {
                newEmail = user.data.email;
            }
            if (!newCity) {
                newCity = user.data.city;
            }

            await axios.put(`/users/${id}`, {
                id: id,
                name: newName,
                email: newEmail,
                city: newCity,
            })
            alert("User modified!");
            window.location.reload();
        });
    } catch (err) { alert(err); }
}

const deleteUser = document.querySelector('.delete-btn')
    .addEventListener('click', async () => {
        const id = document.getElementById('id-input').value;
        if (!id) {
            alert('Pls enter an ID to search');
        } else if (id < 1) {
            alert('ID is invalid');
        }
        else {
            try {
                await axios.delete(`/users/${id}`)
                alert("User deleted!");
                window.location.reload();
            }
            catch (err) { alert(err); }
        }
    });


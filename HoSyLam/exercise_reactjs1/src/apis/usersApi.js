import axiosClient from '../utilities/axiosClient'

const usersApi = {
  getAllUsers: (params) => {
    const url = '/users'
    return axiosClient.get(url, { params })
  },

  getUserByID: (id) => {
    const url = `/users/${id}`
    return axiosClient.get(url)
  },

  postUser: (params) => {
    const url = `/users`
    return axiosClient.post(url, params)
  },

  putUser: (id, params) => {
    const url = `/users/${id}`
    return axiosClient.put(url, params)
  },

  deleteUser: (id) => {
    return axiosClient.delete(`/users/${id}`)
  },
}

export default usersApi

import React, { useState } from 'react'
import { IconContext } from 'react-icons'
import * as FaIcons from 'react-icons/fa'
import * as HrIcons from 'react-icons/hi'
import * as GiIcons from 'react-icons/gi'
import ADMIN_PHOTO from '../../../assets/img/admin.jpg'
import './Navbar.scss'

export default function Navbar(props) {
  const [sidebar, setSidebar] = useState(true)
  const [admin, setAdmin] = useState(true)
  const [logoText, setLogoText] = useState('AdminLTE')
  const [logoStatus, setLogoStatus] = useState(true)

  function showSidebarAndAdmin() {
    setAdmin(!admin)
    setSidebar(!sidebar)
    props.onShow(!sidebar)
  }

  function showSidebar() {
    setSidebar(!sidebar)
    props.onShow(!sidebar)
  }

  function changeLogoText() {
    logoStatus ? setLogoText('ALT') : setLogoText('AdminLTE')
    setLogoStatus(!logoStatus)
  }

  return (
    <IconContext.Provider value={{ color: '#fff' }}>
      <div className="navbar">
        <div className="left-side">
          <h2 className={!admin ? 'inactive' : undefined}>{logoText}</h2>
          <a
            href="/#"
            onClick={() => {
              changeLogoText()
              showSidebarAndAdmin()
            }}
            className="menu-bars"
          >
            <FaIcons.FaBars />
          </a>
        </div>

        <div className="right-side">
          <a href="/#" onClick={showSidebar} className="menu-bars below">
            <FaIcons.FaBars />
          </a>

          <div className="right-items">
            <a href="/#" className="navbar-element notif-icon">
              <span className="notif-content green-icon">4</span>
              <HrIcons.HiOutlineMail />
            </a>
            <a href="/#" className="navbar-element notif-icon">
              <span className="notif-content yellow-icon">10</span>
              <FaIcons.FaRegBell />
            </a>
            <a href="/#" className="navbar-element notif-icon">
              <span className="notif-content red-icon">9</span>
              <FaIcons.FaRegFlag />
            </a>
            <a href="/#" className="navbar-element profile">
              <img src={ADMIN_PHOTO} alt="profile-avatar" />
              <p>Alexander Pierce</p>
            </a>
            <a href="/#" className="navbar-element">
              <GiIcons.GiGears />
            </a>
          </div>
        </div>
      </div>
    </IconContext.Provider>
  )
}

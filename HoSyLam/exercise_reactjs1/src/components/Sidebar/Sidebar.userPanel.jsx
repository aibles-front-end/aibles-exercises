import React from 'react'
import * as FaIcons from 'react-icons/fa'
import adminPhoto from '../../assets/img/admin.jpg'

export default function UserPanel(props) {
  return (
    <div className="user-panel">
      <img
        src={adminPhoto}
        className={!props.sidebar ? 'img-circle inactive' : 'img-circle'}
        alt="user-img"
      ></img>
      <div className={!props.sidebar ? 'info inactive' : 'info'}>
        <h3>Alexander Pierce</h3>
        <span>
          <FaIcons.FaCircle /> <p>Online</p>
        </span>
      </div>
    </div>
  )
}

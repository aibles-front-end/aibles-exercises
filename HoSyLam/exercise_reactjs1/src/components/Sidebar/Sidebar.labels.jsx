import React from 'react'
import * as FaIcons from 'react-icons/fa'

export default function Labels(props) {
  return (
    <>
      <li className="label">
        <a href="/#">
          <FaIcons.FaRegCircle className="red-icon" />
          {props.sidebar && <span>Important</span>}
        </a>
      </li>
      <li className="label">
        <a href="/#">
          <FaIcons.FaRegCircle className="yellow-icon" />
          {props.sidebar && <span>Warning</span>}
        </a>
      </li>
      <li className="label">
        <a href="/#">
          <FaIcons.FaRegCircle className="blue-icon" />
          {props.sidebar && <span>Information</span>}
        </a>
      </li>
    </>
  )
}

import React from 'react'
import { Menu } from 'antd'
import TitleAndIcons from './Sidebar.mainNav.titleAndIcons'
import * as Icons from '@ant-design/icons'
import * as FaIcons from 'react-icons/fa'
import 'antd/dist/antd.css'

const { SubMenu } = Menu

export default function MainNav(props) {
  // open table subMenu by default
  const [openKeys, setOpenKeys] = React.useState(['sub7'])

  // if a subMenu is opened, another will be closed
  const onOpenChange = (keys) => {
    const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1)
    setOpenKeys(latestOpenKey ? [latestOpenKey] : [])
  }

  return (
    <Menu
      openKeys={openKeys}
      onOpenChange={onOpenChange}
      theme="dark"
      mode={props.sidebar ? 'inline' : undefined} //if subMenu is toggled or not
    >
      <SubMenu
        key="sub1"
        icon={<Icons.DashboardFilled />}
        title={<TitleAndIcons title="Dashboard" caret="true" />}
      >
        <Menu.Item key="1" icon={<FaIcons.FaRegCircle />}>
          Dashboard v1
        </Menu.Item>
        <Menu.Item key="2" icon={<FaIcons.FaRegCircle />}>
          Dashboard v2
        </Menu.Item>
      </SubMenu>

      <SubMenu
        icon={<Icons.CopyOutlined />}
        key="sub2"
        title={
          <TitleAndIcons title="Layouts" color1="blue-bg-icon" content1="4" />
        }
      ></SubMenu>

      <SubMenu
        icon={<Icons.WindowsOutlined />}
        key="sub3"
        name="widgetOpened"
        title={
          <TitleAndIcons
            title="Widgets"
            color1="green-bg-icon"
            content1="new"
          />
        }
      ></SubMenu>

      <SubMenu
        icon={<Icons.PieChartFilled />}
        key="sub4"
        title={<TitleAndIcons title="Charts" caret="true" />}
      >
        <Menu.Item key="3" icon={<FaIcons.FaRegCircle />}>
          ChartJS
        </Menu.Item>
        <Menu.Item key="4" icon={<FaIcons.FaRegCircle />}>
          Morris
        </Menu.Item>
        <Menu.Item key="5" icon={<FaIcons.FaRegCircle />}>
          Plot twist
        </Menu.Item>
        <Menu.Item key="6" icon={<FaIcons.FaRegCircle />}>
          Inline charts
        </Menu.Item>
      </SubMenu>

      <SubMenu
        icon={<Icons.FundProjectionScreenOutlined />}
        key="sub5"
        title={<TitleAndIcons title="UI Elements" caret="true" />}
      >
        <Menu.Item key="7" icon={<FaIcons.FaRegCircle />}>
          General
        </Menu.Item>
        <Menu.Item key="8" icon={<FaIcons.FaRegCircle />}>
          Icon
        </Menu.Item>
        <Menu.Item key="9" icon={<FaIcons.FaRegCircle />}>
          Buttons
        </Menu.Item>
        <Menu.Item key="10" icon={<FaIcons.FaRegCircle />}>
          Sliders
        </Menu.Item>
      </SubMenu>

      <SubMenu
        icon={<Icons.FormOutlined />}
        key="sub6"
        title={<TitleAndIcons title="Forms" caret="true" />}
      >
        <Menu.Item key="11" icon={<FaIcons.FaRegCircle />}>
          General Elements
        </Menu.Item>
        <Menu.Item key="12" icon={<FaIcons.FaRegCircle />}>
          Advanced Elements
        </Menu.Item>
        <Menu.Item key="13" icon={<FaIcons.FaRegCircle />}>
          Editors
        </Menu.Item>
      </SubMenu>

      <SubMenu
        icon={<Icons.TableOutlined />}
        key="sub7"
        title={<TitleAndIcons title="Tables" caret="true" />}
      >
        <Menu.Item key="14" icon={<FaIcons.FaRegCircle />}>
          Simple tables
        </Menu.Item>
        <Menu.Item key="15" icon={<FaIcons.FaRegCircle />}>
          Data tables
        </Menu.Item>
      </SubMenu>

      <SubMenu
        icon={<Icons.CalendarOutlined />}
        key="sub8"
        title={
          <TitleAndIcons
            title="Calendar"
            color1="blue-bg-icon"
            color2="red-bg-icon"
            content1="17"
            content2="3"
          />
        }
      ></SubMenu>

      <SubMenu
        icon={<Icons.MailFilled />}
        key="sub9"
        title={
          <TitleAndIcons
            title="Mailbox"
            color1="red-bg-icon"
            color2="green-bg-icon"
            color3="blue-bg-icon"
            content1="5"
            content2="6"
            content3="12"
          />
        }
      ></SubMenu>

      <SubMenu
        icon={<Icons.FolderFilled />}
        key="sub10"
        title={<TitleAndIcons title="Examples" caret="true" />}
      >
        <Menu.Item key="16" icon={<FaIcons.FaRegCircle />}>
          Example
        </Menu.Item>
        <Menu.Item key="17" icon={<FaIcons.FaRegCircle />}>
          Example
        </Menu.Item>
      </SubMenu>

      <SubMenu
        icon={<Icons.ShareAltOutlined />}
        key="sub11"
        title={<TitleAndIcons title="Multilevel" caret="true" />}
      >
        <Menu.Item key="18" icon={<FaIcons.FaRegCircle />}>
          Level One
        </Menu.Item>
        <SubMenu
          key="sub11.1"
          title="Level One"
          icon={<FaIcons.FaRegCircle style={{ margin: '0 10px 0 48px' }} />}
        >
          <Menu.Item key="19" icon={<FaIcons.FaRegCircle />}>
            Level Two
          </Menu.Item>
          <Menu.Item key="20" icon={<FaIcons.FaRegCircle />}>
            Level Two
          </Menu.Item>
          <Menu.Item key="21" icon={<FaIcons.FaRegCircle />}>
            Level Two
          </Menu.Item>
        </SubMenu>
        <Menu.Item key="22" icon={<FaIcons.FaRegCircle />}>
          Level One
        </Menu.Item>
      </SubMenu>

      <SubMenu
        icon={<Icons.BookFilled />}
        key="sub12"
        title={'Documentation'}
      ></SubMenu>
    </Menu>
  )
}

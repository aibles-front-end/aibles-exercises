import React from 'react'
import * as FaIcons from 'react-icons/fa'

export default function UserPanel(props) {
  return (
    <form
      action=""
      className={!props.sidebar ? 'sidebar-form inactive' : 'sidebar-form'}
    >
      <div className="input-group">
        <input
          type="text"
          className="form-control"
          placeholder="Search..."
        ></input>
        <span className="input-group-btn">
          <button
            type="submit"
            name="search"
            id="search-btn"
            className="btn btn-flat"
          >
            <span className="search-icon">
              <FaIcons.FaSearch />
            </span>
          </button>
        </span>
      </div>
    </form>
  )
}

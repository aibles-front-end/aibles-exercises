import React from 'react'
import UserPanel from './Sidebar.userPanel'
import Form from './Sidebar.form'
import MainNav from './Sidebar.mainNav'
import Labels from './Sidebar.labels'
import './Sidebar.scss'

function Sidebar(props) {
  return (
    <>
      <nav className={props.sidebar ? 'side-bar active' : 'side-bar'}>
        <UserPanel sidebar={props.sidebar} />

        <Form sidebar={props.sidebar} />

        <ul className="side-bar-items">
          <li className={props.sidebar ? 'header' : 'header inactive'}>
            MAIN NAVIGATION
          </li>
          <MainNav sidebar={props.sidebar} />

          <li className={props.sidebar ? 'header' : 'header inactive'}>
            LABELS
          </li>
          <Labels sidebar={props.sidebar} />
        </ul>
      </nav>
    </>
  )
}

export default Sidebar

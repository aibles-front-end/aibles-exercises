import React from 'react'
import * as Icons from '@ant-design/icons'

export default function TitleAndIcons(props) {
  return (
    <div className="title-and-icon">
      <div>{props.title}</div>
      {props.caret === 'true' ? (
        <Icons.CaretLeftOutlined />
      ) : (
        <div className="notif-icons">
          <span className={`notif-icon ${props.color1}`}>{props.content1}</span>

          {props.color2 && (
            <span className={`notif-icon ${props.color2}`}>
              {props.content2}
            </span>
          )}

          {props.color3 && (
            <span className={`notif-icon ${props.color3}`}>
              {props.content3}
            </span>
          )}
        </div>
      )}
    </div>
  )
}

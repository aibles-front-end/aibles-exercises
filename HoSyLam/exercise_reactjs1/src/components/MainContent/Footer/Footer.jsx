import React from 'react'
import './Footer.scss'

export default function Footer() {
  return (
    <footer className="page-footer font-small elegant-color">
      <div className="footer-copyright text-center">
        <strong>
          Copyright © 2014-2019
          <a href="/#"> AdminLTE</a>.
        </strong>{' '}
        All rights reserved.
      </div>
      <div className="version">
        <b>Version</b> 2.4.13
      </div>
    </footer>
  )
}

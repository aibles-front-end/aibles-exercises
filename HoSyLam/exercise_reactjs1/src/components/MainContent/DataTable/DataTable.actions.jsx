import React, { useState } from 'react'
import { Popconfirm, notification } from 'antd'
import AddAndEditUser from './DataTable.addAndEditUser'
import usersApi from '../../../apis/usersApi'
import * as FaIcons from 'react-icons/fa'
import 'antd/dist/antd.css'

export default function Actions(props) {
  const [editModalOpened, setEditModalOpened] = useState(false)

  function confirm(e) {
    deletePost(props.row.values.id)
    e.stopPropagation()
  }

  const deletePost = async (id) => {
    try {
      await usersApi.deleteUser(id)
      props.handleDeletion(id)
      openNotification('bottomLeft')
    } catch (error) {
      openNotification('bottomLeft', error)
    }
  }

  function hideModal() {
    setEditModalOpened(false)
  }

  const openNotification = (placement, error) => {
    if (!error) {
      notification.info({
        message: `Yay!`,
        description: 'User has been deleted successfully',
        placement,
        duration: 3,
      })
    } else {
      notification.error({
        message: 'Oops! We got an error!',
        description: `${error}`,
        placement,
        duration: 3,
      })
    }
  }

  return (
    <>
      {editModalOpened && (
        <AddAndEditUser
          name="edited"
          row={props.row.values}
          openModal={editModalOpened}
          hideModal={hideModal}
          handleModification={props.handleModification}
        />
      )}

      <div className="d-flex justify-content-around">
        <div
          className="action-icon"
          onClick={() => {
            setEditModalOpened(true)
          }}
        >
          <FaIcons.FaEdit />
          <span>Edit</span>
        </div>

        <Popconfirm
          title="Are you sure to delete this user?"
          onConfirm={confirm}
          okText="Yes"
          cancelText="No"
        >
          <div className="action-icon">
            <FaIcons.FaTrash />
            <span>Delete</span>
          </div>
        </Popconfirm>
      </div>
    </>
  )
}

import React, { useState } from 'react'
import { Modal, InputGroup, FormControl } from 'react-bootstrap'
import { notification } from 'antd'
import ErrorMessage from './DataTable.errorMsg'
// eslint-disable-next-line
import usersApi from '../../../apis/usersApi'

export default function AddAndEditUser(props) {
  const [user, setUser] = useState({
    name: props.row.name,
    email: props.row.email,
    city: props.row.city,
  })

  const [modalOpened, setModalOpened] = useState(props.openModal)

  const [errorMessage, setErrorMessage] = useState({
    message: '',
    show: false,
  })

  function handleChange(e) {
    const { name, value } = e.target
    setUser((prevValue) => {
      return {
        ...prevValue,
        [name]: value,
      }
    })
  }

  function SaveData() {
    if (!user.name || !user.email || !user.city) {
      showErrorMessage(`Don't leave any fields blank!`)
    } else if (!validateEmail(user.email)) {
      showErrorMessage(`Email is invalid!`)
    } else {
      ;(async () => {
        try {
          props.name === 'added'
            ? await usersApi.postUser(user)
            : await usersApi.putUser(props.row.id, user)
          props.name === 'added'
            ? props.handleAddition(user)
            : props.handleModification(props.row.id, user)
          openNotification('bottomLeft')
        } catch (error) {
          openNotification('bottomLeft', error)
        }
      })()

      closeModal()
    }
  }

  function closeModal() {
    setModalOpened(false)
    hideErrorMessage()
    props.name === 'edited' && props.hideModal()
    setUser(() => {
      return {
        name: props.row.name,
        email: props.row.email,
        city: props.row.city,
      }
    })
  }

  function hideErrorMessage() {
    setErrorMessage((prevValue) => {
      return {
        ...prevValue,
        show: false,
      }
    })
  }

  function showErrorMessage(msg) {
    setErrorMessage(() => {
      return {
        message: msg,
        show: true,
      }
    })
  }

  const openNotification = (placement, error) => {
    if (error) {
      notification.error({
        message: 'Oops! We got an error!',
        description: `${error}`,
        placement,
        duration: 3,
      })
    } else {
      notification.info({
        message: 'Yay!',
        description: `User has been ${props.name} successfully`,
        placement,
        duration: 3,
      })
    }
  }

  function validateEmail(email) {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(String(email).toLowerCase())
  }

  return (
    <>
      {props.name === 'added' && (
        <button
          className="add-btn"
          onClick={() => {
            setModalOpened(true)
          }}
        >
          Add User
        </button>
      )}

      <Modal
        centered
        show={modalOpened}
        backdrop="static"
        onHide={() => {
          setModalOpened(false)
        }}
      >
        <Modal.Header>
          <h3>{props.name === 'added' ? 'Add' : 'Edit'} User data</h3>
        </Modal.Header>

        <Modal.Body>
          {errorMessage.show && (
            <ErrorMessage
              errorMessage={errorMessage}
              hideError={hideErrorMessage}
            ></ErrorMessage>
          )}

          {props.name === 'added' ? (
            <p>Please fill in all the fields below</p>
          ) : (
            <p>You can change the field(s) you want below</p>
          )}

          <InputGroup className="mb-3">
            <FormControl
              name="name"
              defaultValue={props.row.name}
              onChange={handleChange}
              placeholder="Name"
              aria-label="name"
            />
          </InputGroup>
          <InputGroup className="mb-3">
            <FormControl
              name="email"
              defaultValue={props.row.email}
              onChange={handleChange}
              placeholder="Email"
              aria-label="email"
            />
          </InputGroup>
          <InputGroup className="mb-3">
            <FormControl
              name="city"
              defaultValue={props.row.city}
              onChange={handleChange}
              placeholder="City"
              aria-label="city"
            />
          </InputGroup>
        </Modal.Body>

        <Modal.Footer>
          <button onClick={SaveData} className="add-btn">
            {props.name === 'added' ? 'Add' : 'Save'}
          </button>
          <button onClick={closeModal} className="cancel-btn">
            Cancel
          </button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

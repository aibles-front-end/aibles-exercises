import React, { useState } from 'react'
import { Modal, InputGroup, FormControl } from 'react-bootstrap'
import { notification } from 'antd'
import ErrorMessage from './DataTable.errorMsg'
import usersApi from '../../../apis/usersApi'

export default function EditUser(props) {
  const [user, setUser] = useState({
    name: props.row.name,
    email: props.row.email,
    city: props.row.city,
  })

  const [popupOpened, setPopupOpened] = useState(props.openPopup)

  const [errorMessage, setErrorMessage] = useState({
    message: '',
    show: false,
  })

  function handleChange(e) {
    const { name, value } = e.target
    setUser((prevValue) => {
      return {
        ...prevValue,
        [name]: value,
      }
    })
  }

  function SaveData() {
    if (!user.name || !user.email || !user.city) {
      showErrorMessage(`Don't leave any fields blank!`)
    } else if (!validateEmail(user.email)) {
      showErrorMessage(`Email is invalid!`)
    } else {
      ;(async () => {
        try {
          await usersApi.put(props.row.id, user)
          props.getData()
          openNotification('bottomLeft')
        } catch (error) {
          openNotification('bottomLeft', error)
        }
      })()

      closePopup()
    }
  }

  function closePopup() {
    setPopupOpened(false)
    hideErrorMessage()
    props.hidePopup()
    setUser(() => {
      return {
        name: props.row.name,
        email: props.row.email,
        city: props.row.city,
      }
    })
  }

  function hideErrorMessage() {
    setErrorMessage((prevValue) => {
      return {
        ...prevValue,
        show: false,
      }
    })
  }

  function showErrorMessage(msg) {
    setErrorMessage(() => {
      return {
        message: msg,
        show: true,
      }
    })
  }

  const openNotification = (placement, error) => {
    if (error) {
      notification.error({
        message: 'Oops! We got an error!',
        description: `${error}`,
        placement,
        duration: 3,
      })
    } else {
      notification.info({
        message: 'Yay!',
        description: `User has been edited successfully`,
        placement,
        duration: 3,
      })
    }
  }

  function validateEmail(email) {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(String(email).toLowerCase())
  }

  return (
    <>
      <Modal
        show={popupOpened}
        backdrop="static"
        onHide={() => {
          setPopupOpened(false)
        }}
      >
        <Modal.Header>
          <h3>Edit User data</h3>
        </Modal.Header>

        <Modal.Body>
          {errorMessage.show && (
            <ErrorMessage
              errorMessage={errorMessage}
              hideError={hideErrorMessage}
            ></ErrorMessage>
          )}
          <p>You can change the field(s) you want below</p>

          <InputGroup className="mb-3">
            <FormControl
              name="name"
              defaultValue={props.row.name}
              onChange={handleChange}
              placeholder="Name"
              aria-label="name"
              aria-describedby="basic-addon1"
            />
          </InputGroup>
          <InputGroup className="mb-3">
            <FormControl
              name="email"
              defaultValue={props.row.email}
              onChange={handleChange}
              placeholder="Email"
              aria-label="email"
              aria-describedby="basic-addon1"
            />
          </InputGroup>
          <InputGroup className="mb-3">
            <FormControl
              name="city"
              defaultValue={props.row.city}
              onChange={handleChange}
              placeholder="City"
              aria-label="city"
              aria-describedby="basic-addon1"
            />
          </InputGroup>
        </Modal.Body>

        <Modal.Footer>
          <button onClick={SaveData} className="btn btn-success">
            Save
          </button>
          <button onClick={closePopup} className="btn btn-warning">
            Cancel
          </button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

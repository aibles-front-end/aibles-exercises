import React, { useState } from 'react'
import { useAsyncDebounce } from 'react-table'

export const GlobalFilter = ({ filter, setFilter }) => {
  const [value, setValue] = useState(filter)

  const onChange = useAsyncDebounce((value) => {
    setFilter(value || undefined)
  }, 500)
  //we wait for each 0.5s after typing, then it will start searching
  // this will improve the web performance

  return (
    <div className="input-group">
      <span>Search:</span>
      <input
        value={value || ''}
        className="form-control"
        onChange={(e) => {
          setValue(e.target.value)
          onChange(e.target.value)
        }}
      ></input>
    </div>
  )
}

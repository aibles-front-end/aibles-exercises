export const Columns = [
  {
    Header: 'ID',
    Footer: 'ID',
    accessor: 'id',
  },
  {
    Header: 'Name',
    Footer: 'Name',
    accessor: 'name',
  },
  {
    Header: 'Email',
    Footer: 'Email',
    accessor: 'email',
  },
  {
    Header: 'City',
    Footer: 'City',
    accessor: 'city',
  },
]

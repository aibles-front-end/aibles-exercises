import React, { useState, useMemo, useEffect } from 'react'
import {
  useTable,
  useSortBy,
  useGlobalFilter,
  usePagination,
} from 'react-table'
import AddAndEditUser from './DataTable.addAndEditUser'
import Actions from './DataTable.actions'
import Pagination from './DataTable.pagination'
import { Columns } from './DataTable.columns'
import { GlobalFilter } from './DataTable.globalFilter'
import usersApi from '../../../apis/usersApi'
import * as FaIcons from 'react-icons/fa'
import * as AiIcons from 'react-icons/ai'
import * as CgIcons from 'react-icons/cg'
import './DataTable.scss'

export default function DataTable() {
  const [users, setUsers] = useState([])

  async function getData() {
    try {
      const response = await usersApi.getAllUsers()
      setUsers(response)
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  function handleUserDeletion(id) {
    setUsers((prevValue) => prevValue.filter((item) => item.id !== id))
  }
  function handleUserAddition(user) {
    setUsers((prevValue) => [...prevValue, user])
  }
  function handleUserModification(id, user) {
    let usersList = [...users]

    const position = usersList.findIndex((item) => item.id === id)
    user.id = id
    usersList[position] = user

    setUsers(usersList)
  }

  const columns = useMemo(() => Columns, [])
  const data = useMemo(() => users, [users])

  const tableInstance = useTable(
    {
      columns,
      data,
    },
    useGlobalFilter,
    useSortBy,
    usePagination,
    (hooks) => {
      hooks.visibleColumns.push((columns) => {
        return [
          ...columns, //render data columns first,
          {
            //then render actions columns
            id: 'click',
            Header: () => <span className="ms-2">Actions</span>,
            Cell: ({ row }) => (
              <Actions
                row={row}
                handleDeletion={handleUserDeletion}
                handleModification={handleUserModification}
              />
            ),
            Footer: () => <span className="ms-2">Actions</span>,
          },
        ]
      })
    },
  )

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    footerGroups,
    prepareRow,
    state,
    setGlobalFilter,
    page,
    nextPage,
    canNextPage,
    previousPage,
    canPreviousPage,
    pageOptions,
    pageSize,
    setPageSize,
  } = tableInstance

  const { globalFilter } = state,
    { pageIndex } = state

  return (
    <content>
      <div className="table-title">
        <div className="left-side">
          <h3>Data Tables</h3> <span>advanced tables</span>
        </div>
        <div className="right-side">
          <a href="/#">
            <AiIcons.AiFillDashboard /> Home
          </a>
          <span> {'>'} </span>
          <a href="/#">Table</a>
          <span> {'>'} </span>
          <a href="/#">Data tables</a>
        </div>
      </div>

      <div className="table-and-features">
        <div className="header-and-btn">
          <h4>Data Table With Full Features</h4>
          <AddAndEditUser
            handleAddition={handleUserAddition}
            row={''}
            name="added"
          />
        </div>
        <div className="show-and-search">
          <span>
            Show
            <select
              value={pageSize}
              onChange={(e) => {
                setPageSize(Number(e.target.value))
              }}
            >
              {[10, 25, 50].map((pageSize) => (
                <option key={pageSize} value={pageSize}>
                  {pageSize}
                </option>
              ))}
            </select>
            entries
          </span>

          <GlobalFilter filter={globalFilter} setFilter={setGlobalFilter} />
        </div>

        <div className="table-responsive-sm">
          <table {...getTableProps()}>
            <thead>
              {headerGroups.map((headerGroup) => (
                <tr {...headerGroup.getHeaderGroupProps()}>
                  {headerGroup.headers.map((column) => (
                    <th
                      {...column.getHeaderProps(column.getSortByToggleProps())}
                    >
                      {column.render('Header')}
                      <span>
                        {column.isSorted ? (
                          column.isSortedDesc ? (
                            <FaIcons.FaSortAmountUp />
                          ) : (
                            <FaIcons.FaSortAmountDownAlt />
                          )
                        ) : (
                          <CgIcons.CgArrowsExchangeAltV
                            style={{ fontSize: '22px', marginTop: '2px' }}
                          ></CgIcons.CgArrowsExchangeAltV>
                        )}
                      </span>
                    </th>
                  ))}
                </tr>
              ))}
            </thead>
            <tbody {...getTableBodyProps()}>
              {page.map((row) => {
                prepareRow(row)
                return (
                  <tr {...row.getRowProps()}>
                    {row.cells.map((cell) => {
                      return (
                        <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                      )
                    })}
                  </tr>
                )
              })}
            </tbody>
            <thead>
              {footerGroups.map((footerGroup) => (
                <tr {...footerGroup.getHeaderGroupProps()}>
                  {footerGroup.headers.map((column) => (
                    <th {...column.getHeaderProps()}>
                      {column.render('Footer')}
                    </th>
                  ))}
                </tr>
              ))}
            </thead>
          </table>
        </div>

        <div className="pagination-section">
          {canNextPage ? (
            <span>
              Showing {(pageIndex + 1) * 10 - 9} to {(pageIndex + 1) * 10} of{' '}
              {pageOptions.length} entries
            </span>
          ) : (
            <span>Showing the last entry of {pageOptions.length} entries</span>
          )}

          <Pagination
            nextPage={nextPage}
            canNextPage={canNextPage}
            previousPage={previousPage}
            canPreviousPage={canPreviousPage}
          />
        </div>
      </div>
    </content>
  )
}

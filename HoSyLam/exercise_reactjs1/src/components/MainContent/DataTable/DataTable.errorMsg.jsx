import React, { useState } from 'react'
import { Alert } from 'react-bootstrap'
import * as GrIcons from 'react-icons/gr'

export default function ErrorMessage(props) {
  const [show, setShow] = useState(props.errorMessage.show)

  if (show) {
    return (
      <Alert variant="danger" onClose={() => setShow(false)}>
        <div className="d-flex justify-content-between">
          <h5 className="mt-1">{props.errorMessage.message}</h5>
          <button
            className="btn"
            onClick={() => {
              setShow(false)
              props.hideError()
            }}
          >
            <GrIcons.GrClose />
          </button>
        </div>
      </Alert>
    )
  }
  return null
}

import React from 'react'

export default function Pagination(props) {
  return (
    <ul className="table-pagination">
      <li>
        <a
          className="prev-btn"
          onClick={() => props.previousPage()}
          disabled={!props.canPreviousPage}
          href="/#"
        >
          Previous
        </a>
      </li>
      <li>
        <a href="/#" className="active">
          1
        </a>
      </li>
      <li>
        <a href="/#">2</a>
      </li>
      <li>
        <a href="/#">3</a>
      </li>
      <li>
        <a
          className="next-btn"
          onClick={() => props.nextPage()}
          disabled={!props.canNextPage}
          href="/#"
        >
          Next
        </a>
      </li>
    </ul>
  )
}

import React, { useState } from 'react'
import Navbar from '../components/Header/Navbar/Navbar'
import Sidebar from '../components/Sidebar/Sidebar'
import DataTable from '../components/MainContent/DataTable/DataTable'
import Footer from '../components/MainContent/Footer/Footer'
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.scss'

export default function App() {
  const [sidebar, setSidebar] = useState(true)

  function showSidebar(value) {
    setSidebar(value)
  }

  return (
    <>
      <Navbar onShow={showSidebar} />
      <div className="wrapper">
        <Sidebar sidebar={sidebar} />
        <div className="main-content">
          <DataTable />
          <Footer />
        </div>
      </div>
    </>
  )
}

import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App/App";

const rootElement = document.getElementById("root");
ReactDOM.render(
  <div>
    <App></App>
  </div>,
  rootElement
);
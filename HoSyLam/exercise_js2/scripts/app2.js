let accounts = [
    {
        username: "lam1",
        password: "1",
    },
    {
        username: "son2",
        password: "2",
    },
    {
        username: "long3",
        password: "3",
    },
    {
        username: "huy4",
        password: "4",
    },
];

const registerBtn = document.getElementById('register');

const message = document.querySelector('.message');

registerBtn.addEventListener('click', function (e) {
    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;
    const confirmPassword = document.getElementById('confirm-password').value;

    if (!username || !password || !confirmPassword) {
        setMessage("Username | Password | Confirm password not be blank");
    }
    else if (password !== confirmPassword) {
        setMessage("Password not be match confirm password");
    }
    else {
        let found = false;
        for (acc in accounts) {
            console.log(acc);
            if (accounts[acc].username === username) {
                setMessage("Account already exists");
                found = true;
                break;
            }
        }

        if (!found) {
            alert("Register successfully");
            window.location.reload();
        }
    }

    e.preventDefault();
});

function setMessage(msg) {
    message.style.color = 'red';
    message.textContent = msg;
}
let accounts = [
    {
        username: "lam1",
        password: "1",
    },
    {
        username: "son2",
        password: "2",
    },
    {
        username: "long3",
        password: "3",
    },
    {
        username: "huy4",
        password: "4",
    },
];

const loginBtn = document.getElementById('login');

const message = document.querySelector('.message');

loginBtn.addEventListener('click', function (e) {
    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;

    if (!username || !password) {
        setMessage("Username | Password not be blank");
    }
    else {
        let found = false;
        for (acc in accounts) {
            if (accounts[acc].username === username) {
                if (accounts[acc].password === password) {
                    alert("Login successfully");
                    window.location.reload();
                } else {
                    setMessage("Invalid username/password");
                }
                found = true;
                break;
            }
        }
        if (!found) {
            setMessage("Account not found");
        }
    }

    // e.preventDefault();
});

function setMessage(msg) {
    message.style.color = 'red';
    message.textContent = msg;
}
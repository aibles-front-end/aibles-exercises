
axios.defaults.baseURL = 'https://60becf8e6035840017c17a48.mockapi.io/api';

function getData (){
    axios.get('/users')
        .then ((response) => {
            renderData(response.data);
        })
        .catch ((error) => {
            console.error(error);
        })  
}
getData()

function renderData (users) {
    const tbody = document.getElementById("tbody");
    tbody.innerHTML = ''
    users.map((user, index) => 
        tbody.innerHTML += (`
        <tr key=${index}>
            <td>${user.id}</td>
            <td>${user.name}</td>
            <td>${user.email}</td>
            <td>${user.city}</td>
            <div class="">
                <button onclick="handleDelete(${user.id})" class="mx-5 mb-3 btn btn-danger">Delete</button>
                <button onclick="handleEdit(${user.id})" class="mx-5 mb-3 btn btn-primary">Edit</button>
            </div>
        </tr>
        `
        ))
}

function handleAdd() {
    const name = document.getElementById('name').value
    const email = document.getElementById('email').value
    const city = document.getElementById('city').value
        
    if (!name || !email || !city){
        alert("Please enter full information")
    }
    else{
        axios.post('/users', {
            name: name,
            avatar: "https://cdn.fakercloud.com/avatars/d_kobelyatsky_128.jpg",
            email: email,
            city: city,
            image: "http://placeimg.com/640/480"
        })
            .then((response) => {
                getData()
                console.log(response.data);
                alert('Success!')
            })
            .catch((error) => {
                alert(error);
            });
        }
}

function handleDelete (id){
    axios.delete('/users'+'/'+id)
        .then((response) => {
            getData()
            console.log(response.data);
            alert('Delete Successfully!')
        })
        .catch((error) => {
            alert(error);
        });
}

function handleEdit(id){
    const nameEdit = document.getElementById('nameEdit').value
    const emailEdit = document.getElementById('emailEdit').value
    const cityEdit = document.getElementById('cityEdit').value
    
    axios.put('/users/'+id, {
        name: nameEdit,
        email: emailEdit,
        city: cityEdit
    })
        .then((response) => {
            getData()
            console.log(response.data);
        })
        .catch((error) => {
            alert(error);
        });
}

import React from "react";
import { Modal, Button, Form, Input } from "antd";
import { toast } from "react-toastify";
import { getData, postUser, putUser } from "../../api/user";
import "antd/dist/antd.css";
import "./ModalCoponent.scss";

const ModalComponent = (props) => {
    const onFinish = (data) => {
        if (props.modalAdd) {
            props.setModalAdd(false);
            postUser(data)
                .then(() => renderData())
                .catch(() => toast.error("Add user failed!"));
        } else {
            props.setModalEdit(null);
            putUser(data, props.modalEdit.id)
                .then(() => renderData())
                .catch(() => toast.error("Edit user failed!"));
        }
    };

    const renderData = () => {
        getData()
            .then((res) => {
                props.setUsers(res.data);
            })
            .then(() => {
                toast.success(
                    props.modalEdit
                        ? "Edit user successfully!"
                        : "Add user successfully!"
                );
                props.modalEdit
                    ? props.setModalEdit(null)
                    : props.setModalAdd(false);
            })
            .catch((err) => alert(err));
    };

    return (
        <Modal
            centered
            visible={props.modalAdd || props.modalEdit}
            onCancel={() => {
                props.setModalAdd(false);
                props.setModalEdit(null);
            }}
            footer={[]}
            destroyOnClose={true}
        >
            <Form
                name="basic"
                labelCol={{ span: 5 }}
                wrapperCol={{ span: 17 }}
                initialValues={
                    props.modalEdit || { name: "", email: "", city: "" }
                }
                onFinish={onFinish}
            >
                <h2 className="title-modal">
                    {props.modalEdit ? "EDIT USER" : "ADD USER"}
                </h2>
                <Form.Item
                    label="Name"
                    name="name"
                    rules={[
                        {
                            required: true,
                            message: "Please enter the name!"
                        }
                    ]}
                >
                    <Input className="inp-text" />
                </Form.Item>

                <Form.Item
                    label="Email"
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: "Please enter the email!"
                        }
                    ]}
                >
                    <Input className="inp-text" type="email" />
                </Form.Item>

                <Form.Item
                    label="City"
                    name="city"
                    rules={[
                        {
                            required: true,
                            message: "Please enter the city!"
                        }
                    ]}
                >
                    <Input className="inp-text" />
                </Form.Item>

                <div className="btn-form">
                    <Button
                        className="btn-submit btn"
                        type="primary"
                        htmlType="submit"
                    >
                        {props.modalEdit ? "Save" : "Submit"}
                    </Button>
                    <Button
                        className="btn-cancel btn"
                        onClick={() => {
                            props.setModalAdd(false);
                            props.setModalEdit(null);
                        }}
                    >
                        Cancel
                    </Button>
                </div>
            </Form>
        </Modal>
    );
};

export default ModalComponent;

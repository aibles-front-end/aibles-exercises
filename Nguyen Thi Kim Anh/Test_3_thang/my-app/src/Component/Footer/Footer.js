import React from "react";
import "./Footer.scss";

export default function Footer() {
    return (
        <footer>
            <div className="text-left">
                <strong>
                    Copyright © 2014-2019
                    <a href="#"> AdminLTE</a>.
                </strong>
                All rights reserved.
            </div>
            <div className="text-right">
                <b>Version </b>2.4.13
            </div>
        </footer>
    );
}

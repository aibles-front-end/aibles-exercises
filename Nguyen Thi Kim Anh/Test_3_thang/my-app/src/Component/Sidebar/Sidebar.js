import React from "react";
import {
    FaSearch,
    AiFillDashboard,
    IoChevronBack,
    VscFiles,
    FaTh,
    FaChartPie,
    MdComputer,
    FaEdit,
    BsTable,
    FaCalendarAlt,
    HiOutlineMail,
    FaFolder,
    RiShareForwardFill,
    FaBook,
    BiCircle,
    FaCircle
} from "react-icons/all";
import SidebarItem from "./SidebarItem";
import "./Sidebar.scss";

export default function Sidebar() {
    return (
        <aside className="sidebar">
            <div className="user-panel">
                <div>
                    <img src="https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg" />
                </div>
                <div className="info">
                    <p>Alexander Pierce</p>
                    <a href="#" role="button">
                        <FaCircle className="icon-online" />
                        <span>Online</span>
                    </a>
                </div>
            </div>

            <form className="sidebar-form">
                <div className="input-group">
                    <input
                        type="text"
                        className="input-search"
                        placeholder="Search..."
                    />
                    <span className="btn-search ">
                        <button type="submit" name="search">
                            <FaSearch />
                        </button>
                    </span>
                </div>
            </form>

            <div className="header-sidebar">MAIN NAVIGATION</div>
            <ul className="sidebar-menu">
                <SidebarItem
                    iconLeft={<AiFillDashboard className="icon-left" />}
                    title="Dashboard"
                    iconRight={<IoChevronBack className="icon-right" />}
                />
                <SidebarItem
                    iconLeft={<VscFiles className="icon-left" />}
                    title="Layout Options"
                    iconRight={<div className="label label-primary">4</div>}
                />
                <SidebarItem
                    iconLeft={<FaTh className="icon-left" />}
                    title="Widgets"
                    iconRight={<div className="label label-success">new</div>}
                />
                <SidebarItem
                    iconLeft={<FaChartPie className="icon-left" />}
                    title="Charts"
                    iconRight={<IoChevronBack className="icon-right" />}
                />
                <SidebarItem
                    iconLeft={<MdComputer className="icon-left" />}
                    title="UI Elements"
                    iconRight={<IoChevronBack className="icon-right" />}
                />
                <SidebarItem
                    iconLeft={<FaEdit className="icon-left" />}
                    title="Forms"
                    iconRight={<IoChevronBack className="icon-right" />}
                />
                <SidebarItem
                    iconLeft={<BsTable className="icon-left" />}
                    title="Tables"
                    iconRight={<IoChevronBack className="icon-right" />}
                />
                <SidebarItem
                    iconLeft={<FaCalendarAlt className="icon-left" />}
                    title="Calender"
                    iconRight={
                        <div className="number-right">
                            <div className="label label-primary">17</div>
                            <div className="label label-danger">3</div>
                        </div>
                    }
                />
                <SidebarItem
                    iconLeft={<HiOutlineMail className="icon-left" />}
                    title="Mailbox"
                    iconRight={
                        <div className="number-right">
                            <div className="label label-danger">5</div>
                            <div className="label label-success">16</div>
                            <div className="label label-warning">12</div>
                        </div>
                    }
                />
                <SidebarItem
                    iconLeft={<FaFolder className="icon-left" />}
                    title="Examples"
                    iconRight={<IoChevronBack className="icon-right" />}
                />
                <SidebarItem
                    iconLeft={<RiShareForwardFill className="icon-left" />}
                    title="Multilevel"
                    iconRight={<IoChevronBack className="icon-right" />}
                />
                <SidebarItem
                    iconLeft={<FaBook className="icon-left" />}
                    title="Documentation"
                />
            </ul>

            <div className="header-sidebar">LABLES</div>
            <ul className="sidebar-menu">
                <SidebarItem
                    iconLeft={<BiCircle className="icon-circle icon-danger" />}
                    title="Important"
                />
                <SidebarItem
                    iconLeft={<BiCircle className="icon-circle icon-warning" />}
                    title="Warning"
                />
                <SidebarItem
                    iconLeft={<BiCircle className="icon-circle icon-info" />}
                    title="Infomation"
                />
            </ul>
        </aside>
    );
}

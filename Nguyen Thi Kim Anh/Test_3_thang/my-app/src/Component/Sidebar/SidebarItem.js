import React from "react";

export default function SidebarItem(props) {
    return (
        <li className="treeview">
            <a href="#">
                {props.iconLeft}
                <div className="title-item">{props.title}</div>
            </a>
            {props.iconRight}
        </li>
    );
}

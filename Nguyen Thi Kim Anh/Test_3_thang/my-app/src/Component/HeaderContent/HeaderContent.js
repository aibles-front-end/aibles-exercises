import React from "react";
import { AiFillDashboard, AiOutlineRight } from "react-icons/all";
import "./HeaderContent.scss";

export default function HeaderContent() {
    return (
        <div className="content-header">
            <div className="title-header">
                Data Tables
                <small className="advance-tables">advanced tables</small>
            </div>
            <div className="breadcrumb">
                <div className="icon-left">
                    <AiFillDashboard />
                </div>
                <div className="title-link">Home</div>
                <div className="icon-right">
                    <AiOutlineRight />
                </div>
                <div className="title-link">Tables</div>
                <div className="icon-right">
                    <AiOutlineRight />
                </div>
                <div className="text-light">Data tables</div>
            </div>
        </div>
    );
}

import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import {
    FaSortAmountDownAlt,
    AiOutlineDelete,
    FiEdit,
    RiArrowUpDownFill
} from "react-icons/all";
import { Button, Popconfirm, Pagination } from "antd";
import ModalComponent from "../Modal/ModalComponent";
import { getData, deleteUser } from "../../api/user";
import "react-toastify/dist/ReactToastify.css";
import "./MainContent.scss";

export default function MainContent() {
    const [users, setUsers] = useState([]);
    const [modalAdd, setModalAdd] = useState(false);
    const [modalEdit, setModalEdit] = useState(null);
    const [confirmDelete, setConfirmDelete] = useState(null);

    useEffect(() => {
        getUser();
    }, []);

    const getUser = () => {
        getData()
            .then((res) => {
                setUsers(res.data);
            })
            .catch((err) => alert(err));
    };

    const handleOnConfirm = () => {
        deleteUser(confirmDelete)
            .then(() => renderData())
            .catch(() => toast.error("Delete user failed!"));
    };

    const renderData = () => {
        setConfirmDelete(null);
        getData()
            .then((res) => {
                setUsers(res.data);
            })
            .then(() => {
                toast.success("Delete user successfully!");
                setModalEdit(null);
            })
            .catch((err) => alert(err));
    };

    function itemRender(current, type, originalElement) {
        if (type === "prev") {
            return <Button>Previous</Button>;
        }
        if (type === "next") {
            return <Button>Next</Button>;
        }
        return originalElement;
    }

    return (
        <div className="box">
            <div className="header-box">
                <div className="box-title">Hover Data Table</div>
                <Button
                    className="button-add"
                    type="primary"
                    onClick={() => {
                        setModalAdd(true);
                    }}
                >
                    Add user
                </Button>
            </div>

            <div className="box-body">
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col" className="id">
                                Id
                            </th>
                            <th scope="col" className="title-col">
                                <div>Name</div>
                                <FaSortAmountDownAlt className="icon-col" />
                            </th>
                            <th scope="col" className="title-col">
                                <div>Email</div>
                                <RiArrowUpDownFill className="icon-col" />
                            </th>
                            <th scope="col" className="title-col">
                                <div>City</div>
                                <RiArrowUpDownFill className="icon-col" />
                            </th>
                            <th scope="col">Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        {users.map((user, index) => {
                            return (
                                <tr key={index}>
                                    <td className="id">{user.id}</td>
                                    <td>{user.name}</td>
                                    <td>{user.email}</td>
                                    <td>{user.city}</td>
                                    <td>
                                        <div className="icon">
                                            <FiEdit
                                                className="icon-edit"
                                                onClick={() => {
                                                    setModalEdit(user);
                                                }}
                                            />
                                            <Popconfirm
                                                title="Are you sure to delete this user?"
                                                onConfirm={handleOnConfirm}
                                                onCancel={() => {
                                                    setConfirmDelete(null);
                                                }}
                                                okText="Yes"
                                                cancelText="No"
                                                placement="rightTop"
                                            >
                                                <AiOutlineDelete
                                                    className="icon-delete"
                                                    onClick={() => {
                                                        setConfirmDelete(
                                                            user.id
                                                        );
                                                    }}
                                                />
                                            </Popconfirm>
                                        </div>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                    <tfoot>
                        <tr>
                            <th className="id">Id</th>
                            <th>Name</th>
                            <th>City</th>
                            <th>Email</th>
                            <th>Option</th>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <div className="page">
                <div className="data-table-info">
                    Showing 1 to 10 of 57 entries
                </div>
                <Pagination
                    defaultCurrent={1}
                    total={60}
                    itemRender={itemRender}
                    showSizeChanger={false}
                />
            </div>
            <ModalComponent
                modalAdd={modalAdd}
                setModalAdd={setModalAdd}
                modalEdit={modalEdit}
                setModalEdit={setModalEdit}
                setUsers={setUsers}
            />
        </div>
    );
}

import { ToastContainer } from "react-toastify";
import Header from "./Component/Header/Header";
import Sidebar from "./Component/Sidebar/Sidebar";
import HeaderContent from "./Component/HeaderContent/HeaderContent";
import MainContent from "./Component/MainContent/MainContent";
import Footer from "./Component/Footer/Footer";
import "./App.scss";

function App() {
    return (
        <div>
            <Header />
            <div className="container">
                <Sidebar />
                <div className="content">
                    <HeaderContent />
                    <MainContent />
                    <Footer />
                </div>
            </div>
            <ToastContainer />
        </div>
    );
}

export default App;

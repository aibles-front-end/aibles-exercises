let accounts = [
    user1 = {
        username: "KimAnh",
        password: "123"
    },
    user2 = {
        username: "abc",
        password: "111"
    },
    user3 = {
        username: "xyz",
        password: "222"
    }
 
];

document.getElementById('signup').addEventListener('click', () => {
    const username = document.getElementById('register-username').value;
    const password = document.getElementById('register-password').value;
    const cfPassword = document.getElementById('confirm-password').value;

    if (username === '' || password === '' || cfPassword === ''){
        alert("Username | Password | Confirm password not be blank");
    }
    else if (password !== cfPassword){
        alert("Password not be match confirm password")
    }
    else {
        let check = false;
        for (user in accounts){
            if (accounts[user].username === username){
                alert("Account already exists");
                check = true;
                break;
            }
        }
        if(!check){
            alert("Register successfull");
            window.location.reload();
        }
    }
});


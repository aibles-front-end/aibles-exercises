import React from 'react'
import {FaSearch, AiFillDashboard, IoChevronBack, VscFiles, FaTh, FaChartPie, MdComputer, FaEdit, BsTable,
    FaCalendarAlt, HiOutlineMail, FaFolder, RiShareForwardFill, FaBook} from 'react-icons/all';

import './sidebar.scss'

export default function SideBar() {
    return (
        <aside className='sidebar'>
            <div className='user-panel'>
                <div >
                    <img src='https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg'/>
                </div>
                <div className='info'>
                    <p>Alexander Pierce</p>
                    <a href='#' role='button'>
                        <span className='icon-online'></span>
                        <span>Online</span>
                    </a>
                </div>
            </div>
            <form className='sidebar-form'>
                <div>
                    <input type="text" className="input-search" placeholder="Search..." />
                    <span className="btn-search " >
                        <a href='#' role='button'><FaSearch /></a>
                    </span>
                </div>
            </form>
            <div className='header'>MAIN NAVIGATION</div>
            <ul className='sidebar-menu'>
                <li className='treeview'>
                    <a href='#'>
                        <span className='icon-left'><AiFillDashboard /></span>
                        <span className='title-item'>Dashboard</span>
                    </a>
                    <span><IoChevronBack /></span>
                </li>
                <li className='treeview'>
                    <a href='#'>
                        <span className='icon-left'><VscFiles /></span>
                        <span className='title-item'>Layout Options</span>
                    </a>
                    <span className='label label-primary'>4</span>
                </li>
                <li className='treeview'>
                    <a href='#'>
                        <span className='icon-left'><FaTh /></span>
                        <span className='title-item'>Widgets</span>
                    </a>
                    <span className='label label-success'>new</span>
                </li>
                <li className='treeview'>
                    <a href='#'>
                        <span className='icon-left'><FaChartPie /></span>
                        <span className='title-item'>Charts</span>
                    </a>
                    <span><IoChevronBack /></span>
                </li>
                <li className='treeview'>
                    <a href='#'>
                        <span className='icon-left'><MdComputer /></span>
                        <span className='title-item'>UI Elements</span>
                    </a>
                    <span><IoChevronBack /></span>
                </li>
                <li className='treeview'>
                    <a href='#'>
                        <span className='icon-left'><FaEdit /></span>
                        <span className='title-item'>Forms</span>
                    </a>
                    <span><IoChevronBack /></span>
                </li>
                <li className='treeview'>
                    <a href='#'>
                        <span className='icon-left'><BsTable /></span>
                        <span className='title-item'>Tables</span>
                    </a>
                    <span><IoChevronBack /></span>
                </li>
                <li className='treeview'>
                    <a href='#'>
                        <span className='icon-left'><FaCalendarAlt /></span>
                        <span className='title-item'>Calender</span>
                    </a>
                    <div>
                        <span className='label label-primary'>17</span>
                        <span className='label label-danger'>3</span>
                    </div>
                </li>
                <li className='treeview'>
                    <a href='#'>
                        <span className='icon-left'><HiOutlineMail /></span>
                        <span className='title-item'>Mailbox</span>
                    </a>
                    <div>
                        <span className='label label-danger'>5</span>
                        <span className='label label-success'>16</span>
                        <span className='label label-warning'>12</span>
                    </div>
                </li>
                <li className='treeview'>
                    <a href='#'>
                        <span className='icon-left'><FaFolder /></span>
                        <span className='title-item'>Examples</span>
                    </a>
                    <span><IoChevronBack /></span>
                </li>
                <li className='treeview'>
                    <a href='#'>
                        <span className='icon-left'><RiShareForwardFill /></span>
                        <span className='title-item'>Multilevel</span>
                    </a>
                    <span><IoChevronBack /></span>
                </li>
                <li className='treeview'>
                    <a href='#'>
                        <FaBook />
                        <span className='title-item'>Documentation</span>
                    </a>
                </li>
            </ul>
            <div className='header'>LABLE</div>
            <ul className='sidebar-menu'>
                <li className='treeview'>
                    <a href='#'>
                        <span className='icon-circle icon-danger'></span>
                        <span className='title-item'>Important</span>
                    </a>
                </li>
                <li className='treeview'>
                    <a href='#'>
                        <span className='icon-circle icon-warning'></span>
                        <span className='title-item'>Warning</span>
                    </a>
                </li>
                <li className='treeview'>
                    <a href='#'>
                        <span className='icon-circle icon-info'></span>
                        <span className='title-item'>Infomation</span>
                    </a>
                </li> 
            </ul>
        </aside>
    )
}

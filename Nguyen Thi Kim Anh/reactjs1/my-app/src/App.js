
import Header from "./Component/Header/Header";
import SideBar from "./Component/Sidebar/Sidebar";
import HeaderContent from "./Component/HeaderContent/HeaderContent";
import MainContent from "./Component/MainContent/MainContent";
import './App.scss'

function App() {
  return (
    <div>
      <Header />
      <div className='container'>
        <SideBar />
        <div className='content'>
          <HeaderContent />
          <MainContent />
        </div>
      </div>
    </div>
  );
}

export default App;

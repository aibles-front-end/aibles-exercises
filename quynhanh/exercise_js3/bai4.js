const reverseWords = ((text) => {
    return str = text.trim().split(' ').reverse().join(' ').replace(/\s+/g, ' ')
})
console.log(reverseWords(" the sky is blue"))
console.log(reverseWords("hello   world!  "))
console.log(reverseWords("a good example"))
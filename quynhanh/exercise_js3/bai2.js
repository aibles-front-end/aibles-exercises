const formatNumber = ((number) => {
    return number <= 0 ? '0' : number.toLocaleString('en-US')
})
console.log(formatNumber(-1))
console.log(formatNumber(0))
console.log(formatNumber(10))
console.log(formatNumber(1000))
console.log(formatNumber(10000))
console.log(formatNumber(1000000))
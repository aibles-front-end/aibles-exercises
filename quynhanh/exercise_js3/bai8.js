const isPandigital = ((number) => {
    const str = number.toString()
    for(let i = 0; i < 10; i++){
        if(str.includes(i) == false)
            return false;
    }
    return true;
})

console.log(isPandigital(98140723568910) )
console.log(isPandigital(90864523148909))
console.log(isPandigital(2233445566778899))
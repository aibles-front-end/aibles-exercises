const axios = require('axios')
function callAPIbyPromise(){
    axios.get('https://api.github.com/users/ptit9x')
        .then(function (response) {
            console.log(response.data)
        })
        .catch(function (error){
            console.log(error)
        })
}
//callAPIbyPromise()
async function callAPIbyAA(){
    try{
        const response = await axios.get('https://api.github.com/users/ptit9x')
        console.log(response.data)
    }
    catch(error){
        console.log(error)
    }
}
callAPIbyAA()
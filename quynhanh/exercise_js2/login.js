var username = document.getElementById('username')
var password = document.getElementById('password')
var loginBtn = document.getElementById('login-btn')
var confirmBtn = document.getElementById('confirm-btn')
var confirmPass = document.getElementById('confirm-pass')
var confirmInput = document.getElementById('confirm-input')

var users = [
    { name: 'wonwoo', password: '23456'},
    { name: 'mingyu', password: '78910'},
    { name: 'jun', password: 'jqka'},
    { name: 'chan', password: 'joker'}
]

//login
function isExist(name, password){
    for(var i = 0; i < users.length; i++){
        if(users[i].name === name && users[i].password === password){
            return 'Login successful' 
        }
        else if(users[i].name === name && users[i].password !== password){
            return 'Invalid username/password'
        }
    }
    return 'Account not found'
}

function isBlank(){
    if(username.value === '' || password.value === ''){
        alert('Username | Password not be blank')
    }
    else{
        alert(isExist(username.value, password.value))
        if(isExist(username.value, password.value) === 'Login successful'){
            confirmInput.style.display = 'block'
            loginBtn.style.display = 'none'
            confirmBtn.style.display = 'block'
        }
    }
}

//register
function isBlankConfirm(){
    if(username.value === '' || password.value === '' || confirmPass.value === ''){
        alert('Username | Password | Confirm password not be blank')
    }
}

function confirmText(obj, text){
    obj.classList.add('warning')
    obj.innerHTML = text
}

function checkConfirm(){
    var warningUsername = document.querySelector('.warning-username')
    var warningPass = document.querySelector('.warning-password')
    if(confirmPass.value !== password.value){
        confirmText(warningPass, 'Password not be match confirm password')
    }
    else{
        confirmText(warningPass, '')
        alert('Register successfully')
    }
    for(var i = 0; i < users.length; i++){
        if(users[i].name === username.value){
            confirmText(warningUsername, 'Account already exists')
            break;
        }
    }
}

//add event listener
loginBtn.addEventListener('click', function(){
    isBlank()
})

confirmBtn.addEventListener('click', function(){
    isBlankConfirm()
    checkConfirm()
})
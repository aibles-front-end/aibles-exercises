import { useState } from "react"
import { getUser, postUser, putUser } from "../../apis/userApi"
import { Form, Input, Button, Modal, notification } from "antd"
import "antd/dist/antd.css"

const ModalUser = (props) => {
  const [addModal, setAddModal] = useState(false)
  const [email, setEmail] = useState("")

  const onFinishModal = (user) => {
    if (addModal) {
      setAddModal(false)
      postUser(user)
        .then(() => displayData())
        .catch(() => {
          notification["error"]({
            message: "Add user failed",
            placement: "topRight"
          })
        })
    }
    if (props.editModal) {
      props.setEditModal(null)
      putUser(user, props.editModal.id)
        .then(() => displayData())
        .catch(() => {
          notification["error"]({
            message: "Edit user failed",
            placement: "topRight"
          })
        })
    }
  }

  const displayData = () => {
    getUser()
      .then((response) => {
        props.setUsers(response.data)
        notification["success"]({
          message: addModal ? "Add user successful" : "Edit user successful",
          placement: "topRight"
        })
        props.editModal ? props.setEditModal(null) : setAddModal(false)
      })
      .catch((error) => console.log(error))
  }

  const onCancelModal = () => {
    setAddModal(false)
    props.setEditModal(null)
  }

  const validateEmail = {
    required: "Email is required!",
    types: {
      email: `${email} is not a valid email!`
    }
  }

  return (
    <>
      <Button
        type="primary"
        style={{ marginBottom: 20, fontSize: 14 }}
        onClick={() => setAddModal(true)}
      >
        Add user
      </Button>
      <Modal
        title={addModal ? "Add user" : "Edit user"}
        visible={addModal || props.editModal}
        onCancel={onCancelModal}
        footer={""}
        destroyOnClose={true}
      >
        <Form
          name="nest-messages"
          labelCol={{ span: 5 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinishModal}
          initialValues={props.editModal}
          validateEmail={validateEmail}
        >
          <Form.Item
            label="Name"
            name="name"
            rules={[{ required: true, message: "Please input your username!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Email"
            name="email"
            onChange={(evt) => setEmail(evt.target.value)}
            rules={[{ required: true, type: "email" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="City"
            name="city"
            rules={[{ required: true, message: "Please input your city!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 8, span: 16 }} className="form-btn">
            <Button style={{ marginRight: 10 }} onClick={onCancelModal}>
              Close
            </Button>
            <Button type="primary" htmlType="submit" className="btn-submit">
              {addModal ? "Add" : "Save"}
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  )
}

export default ModalUser

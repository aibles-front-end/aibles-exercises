const getUsers = () => {
    const response = axios.get('https://60becf8e6035840017c17a48.mockapi.io/api/users')
    .then((response) => {
        showUsers(response.data)
    })
    .catch((error) => {
        console.log(error)
    })
}

const showUsers = (users) => {
    const body = document.getElementById('body')
    users.map((user) => body.innerHTML += `
            <tr>
                <td>${user.id}</td>
                <td>${user.name}</td>
                <td>${user.email}</td>
                <td>${user.city}</td>
                <td>
                    <button type="button" class="btn btn-primary" id="delete-user" 
                            onclick="deleteUsers(${user.id})"
                    >Delete</button>
                    <button type="button" class="btn btn-primary" id="update-user" 
                            onclick="updateUsers(${user.id})"
                    >Update</button>
                </td>
            </tr>
        `    
    )
}

const btnAdd = document.getElementById('add-user')
const input = document.querySelectorAll('input')
const addUsers = () => {
    const name = input[0].value
    const email = input[1].value
    const city = input[2].value
    if(name === '' || email === '' || city === '') alert('You must complete the input')
    else{
        axios.post('https://60becf8e6035840017c17a48.mockapi.io/api/users', {
            name: name,
            email: email,
            city: city
        })
        .then((response) => {
            getUsers()
            alert('Add successful')
        })
        .catch((error) => {
            console.log(error)
        })
    }
}

const deleteUsers = (userID) => {
    axios.delete(`https://60becf8e6035840017c17a48.mockapi.io/api/users/${userID}`, {
        data: userID
    })
    .then((response) => {
        getUsers()
        window.location.reload()
        alert('Delete successful')
    })
    .catch((error) => {
        console.log(error)
    })
}

const updateUsers = (userID) => {
    const name = input[0].value
    const email = input[1].value
    const city = input[2].value
    axios.put(`https://60becf8e6035840017c17a48.mockapi.io/api/users/${userID}`, {
        id: userID,
        name: name,
        email: email,
        city: city
    })
    .then((response) => {
        getUsers()
        window.location.reload()
        alert('Update successful')
    })
    .catch((error) => {
        console.log(error)
    })
}

getUsers()
btnAdd.addEventListener('click', function(){
    addUsers()
})

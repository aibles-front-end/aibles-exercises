import axiosClient from './axiosClient'
const usersApi = {
  getAll: (params) => {
    const url = '/users'
    return axiosClient.get(url, { params })
  },

  getById: (id) => {
    const url = `/users/${id}`
    return axiosClient.get(url)
  },

  post: (data) => {
    return axiosClient.post(`/users`, data)
  },

  put: (id, data) => {
    return axiosClient.put(`/users/${id}`, data)
  },

  delete: (id) => {
    return axiosClient.delete(`/users/${id}`)
  },
}

export default usersApi

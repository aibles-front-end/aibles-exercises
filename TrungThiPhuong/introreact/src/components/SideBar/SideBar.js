import React, { useState } from 'react'
import './SideBar.scss'
import AVATAR from '../../assets/images/avatar.jpg'

export default function SideBar({ sidebar }) {
  const ItemSidebar = (props) => {
    const { title, iconName } = props
    return (
      <div className="tree-view">
        <a href="#">
          <i className={iconName}></i>
          <span>{title}</span>
        </a>
      </div>
    )
  }

  return (
    <div className={sidebar ? 'sidebar' : 'side-bar-small'}>
      <div className="sidebar_header">
        <div className="user-info">
          <div className="img-avatar">
            <img src={AVATAR} className="circle" alt="qa"></img>
          </div>
          <div className="user-name">
            <p>Alexander Pierce</p>
            <a href="#">
              <i className="fas fa-circle text-success fa-1x"></i>
            </a>
            <span>Online</span>
          </div>
        </div>
        <div className="input-group search">
          <input
            className="form-control rounded"
            placeholder="Search..."
            described="search-addon"
          />
          <span className="input-group-text border-0" id="search-addon">
            <i className="fas fa-search"></i>
          </span>
        </div>
      </div>
      <ul className="components">
        <p>MAIN NAVIGATION</p>
        <li className="active">
          <span className="item-left">
            <ItemSidebar
              iconName="nav-icon fas fa-tachometer-alt icon"
              title="Dashboard"
            ></ItemSidebar>
          </span>
          <span className="item-right">
            <i className="fa fa-angle-left pull-right"></i>
          </span>
        </li>
        <li className="active">
          <span className="item-left">
            <ItemSidebar
              iconName="nav-icon fas fas fa-file icon"
              title="Layout Options"
            ></ItemSidebar>
          </span>
          <span className="item-right">
            <i className="fa fa-angle-left pull-right icon"></i>
          </span>
        </li>
        <li className="active">
          <span className="item-left">
            <ItemSidebar
              iconName="nav-icon fas fas fa-file icon"
              title="Layout Options"
            ></ItemSidebar>
          </span>
          <span className="item-right">
            <i className="fa fa-angle-left pull-right "></i>
          </span>
        </li>
        <li className="active">
          <span className="item-left">
            <ItemSidebar
              iconName="fas fa-th icon"
              title="Widgets"
            ></ItemSidebar>
          </span>
          <span className="item-right">
            <i className="fa fa-angle-left pull-right"></i>
          </span>
        </li>

        <li className="active">
          <span className="item-left">
            <ItemSidebar
              iconName="fas fa-folder icon"
              title="Examples"
            ></ItemSidebar>
          </span>
          <span className="item-right">
            <i className="fa fa-angle-left pull-right"></i>
          </span>
        </li>
      </ul>

      <ul className="list-un-styled components">
        <p>LABEL</p>
        <li className="active">
          <span className="item-left">
            <ItemSidebar
              iconName="far fa-circle text-danger"
              title="Important"
            ></ItemSidebar>
          </span>
        </li>
        <li className="active">
          <span className="item-left">
            <ItemSidebar
              iconName="far fa-circle text-warning"
              title="Warning"
            ></ItemSidebar>
          </span>
        </li>

        <li className="active">
          <span className="item-left">
            <ItemSidebar
              iconName="far fa-circle text-info"
              title="Information"
            ></ItemSidebar>
          </span>
        </li>
      </ul>
    </div>
  )
}

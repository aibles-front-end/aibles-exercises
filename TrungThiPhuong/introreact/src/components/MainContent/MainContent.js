import React, { useState, useEffect } from 'react'
import { Button } from 'react-bootstrap'
import userApi from '../../api/userApi'
import UserData from './UserData'
import FormEditUser from './FormEditUser'
import './MainContent.scss'

function MainContent() {
  const [user, setUser] = useState()
  const [show, setShow] = useState(false)
  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)
  const [editModal, setEditModal] = useState(null)

  const getUserData = (userData) => {
    console.log(userData)
  }

  async function getData() {
    try {
      const response = await userApi.getAll()
      setUser(response)
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  return (
    <div className="main-content">
      <div>
        <h3>Hover Data Table</h3>
      </div>
      <div>
        <Button onClick={handleShow}>
          <i className="material-icons">&#xE147;</i> <span>Add New </span>
        </Button>
      </div>

      <FormEditUser
        show={show}
        handleShow={handleShow}
        handleClose={handleClose}
        getData={getData}
      ></FormEditUser>

      <UserData
        user={user}
        editModal={editModal}
        handleClose={handleClose}
        handleShow={handleShow}
        getUserData={getUserData}
        getData={getData}
      ></UserData>
    </div>
  )
}

export default MainContent

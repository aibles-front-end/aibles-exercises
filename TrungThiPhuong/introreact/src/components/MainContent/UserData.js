import React, { useState } from 'react'
import { Button, Table, Pagination, Modal } from 'react-bootstrap'
import userApi from '../../api/userApi'
import './UserData.scss'

export default function UserData(props) {
  const user = props.user
  const [userId, setUserId] = useState(null)
  const [showConFirm, setShowConFirm] = useState(false)
  const handleCloseConFirm = () => setShowConFirm(false)

  const handleShowConFirm = (id) => {
    setShowConFirm(true)
    setUserId(id)
  }

  let active = 2
  let items = []
  for (let number = 1; number <= 5; number++) {
    items.push(
      <Pagination.Item key={number} active={number === active}>
        {number}
      </Pagination.Item>,
    )
  }

  const deletePost = async (userId) => {
    userApi.delete(userId).then(props.getData()).catch(console.error())
  }

  return (
    <div className="main-table">
      <Modal
        centered
        show={showConFirm}
        onHide={handleCloseConFirm}
        animation={false}
      >
        <Modal.Header closeButton>
          <Modal.Title> Do you want delete user?</Modal.Title>
        </Modal.Header>
        <Modal.Footer>
          <Button
            variant="danger"
            onClick={() => {
              deletePost(userId)
              setShowConFirm(false)
            }}
          >
            Yes
          </Button>
          <Button onClick={() => setShowConFirm(false)}>No</Button>
        </Modal.Footer>
      </Modal>

      <Table striped bordered hover>
        <thead>
          <tr>
            <th scope="col">
              <div>ID</div>
            </th>
            <th scope="col" className="table-col">
              Name
              <i className="fas fa-sort-amount-down"></i>
            </th>
            <th scope="col">
              City
              <i className="fas fa-sort-amount-down-alt"></i>
            </th>
            <th scope="col">
              Email
              <i className="fas fa-sort-amount-down-alt"></i>
            </th>
            <th scope="col">Actions</th>
          </tr>
        </thead>

        <tbody>
          {user &&
            user.map((users) => (
              <tr key={users.id}>
                <td>{users.id}</td>
                <td>{users.name}</td>
                <td>{users.city}</td>
                <td>{users.email}</td>
                <td>
                  <div className="table-action">
                    <Button
                      className="table-button"
                      onClick={() => {
                        props.handleShow()
                        props.getUserData(users)
                      }}
                      variant="primary"
                    >
                      Edit
                    </Button>
                    <Button
                      className="table-button"
                      onClick={() => {
                        handleShowConFirm(users.id)
                      }}
                      variant="danger "
                    >
                      Delete
                    </Button>
                  </div>
                </td>
              </tr>
            ))}
        </tbody>
      </Table>
      <div className="table-footer">
        <div>Showing 1 to 10 of 57 entries</div>
        <Pagination>{items}</Pagination>
      </div>
    </div>
  )
}

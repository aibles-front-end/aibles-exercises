import { getDefaultNormalizer } from '@testing-library/react'
import React, { useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'
import userApi from '../../api/userApi'

export default function FormEditUser(props) {
  const show = props.show
  const handleClose = props.handleClose
  const [user, setUser] = useState({
    name: '',
    city: '',
    email: '',
  })

  function handleChange(e) {
    const { name, value } = e.target
    setUser((prevValue) => {
      return {
        ...prevValue,
        [name]: value,
      }
    })
  }

  function AddData() {
    if (!user.name || !user.email || !user.city) {
      alert(`Don't leave any fields blank!`)
    } else {
      userApi
        .post(user)
        .then(props.getData())
        .catch((error) => console.log(error))
    }
  }

  return (
    <Modal show={show} onHide={handleClose} animation={false} centered>
      <Modal.Header closeButton>
        <Modal.Title> Edit or Add User</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Name</Form.Label>
            <Form.Control
              name="name"
              onChange={handleChange}
              placeholder="Name"
              aria-label="name"
            />
          </Form.Group>

          <Form.Group className="mb-3">
            <Form.Label>City</Form.Label>
            <Form.Control
              name="city"
              onChange={handleChange}
              placeholder="City"
              aria-label="city"
            />
          </Form.Group>

          <Form.Group className="mb-3">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              name="email"
              onChange={handleChange}
              placeholder="Email"
              aria-label="email"
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="outline-danger" onClick={handleClose}>
          Cancel
        </Button>
        <Button
          variant="outline-primary"
          onClick={() => {
            AddData()
            props.handleClose()
          }}
        >
          Save Changes
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

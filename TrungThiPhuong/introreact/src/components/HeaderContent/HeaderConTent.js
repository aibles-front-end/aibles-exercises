import React from 'react'

import './HeaderContent.scss'

export default function HeaderConTent() {
  return (
    <div className="container-fluid">
      <h1 className="title-header">
        Data Tables
        <small className="advance-tables">advanced tables</small>
      </h1>

      <ol className="breadcrumb">
        <li>
          <i className="fas fa-tachometer-alt"></i>
          <span>Home</span>
        </li>

        <li className="active">Table</li>
        <li className="pull-right">Data Table</li>
      </ol>
    </div>
  )
}

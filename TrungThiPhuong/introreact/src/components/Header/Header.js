import React from 'react'
import './Header.scss'
import AVATAR from '../../assets/images/avatar.jpg'

export default function Header({ setSidebar }) {
  const handleShowSide = () => {
    console.log(setSidebar)
    if (setSidebar) setSidebar(false)
    else setSidebar(true)
  }

  return (
    <div className="header">
      <div className="nav-brand">
        <b>Admin</b>
        LTE
      </div>
      <div className="header__inner">
        <button className="btn " onClick={() => handleShowSide()}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            height="20"
            viewBox="0 -53 384 384"
            width="20"
          >
            <g>
              <path
                d="m368 154.667969h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"
                data-original="#000000"
                style={{ fill: '#FFFFFF' }}
                data-old_color="#000000"
              />
              <path
                d="m368 32h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"
                data-original="#000000"
                style={{ fill: '#FFFFFF' }}
                data-old_color="#000000"
              />
              <path
                d="m368 277.332031h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"
                data-original="#000000"
                style={{ fill: '#FFFFFF' }}
                data-old_color="#000000"
              />
            </g>
          </svg>
        </button>
        <div className="menu__right">
          <li className="nav-item ">
            <i className="far fa-envelope"></i>
            <span className="num num-success">4</span>
          </li>
          <li className="nav-item ">
            <i className="far fa-bell"></i>
            <span className="num num-warning">10</span>
          </li>
          <li className="nav-item">
            <i className="far fa-flag"></i>
            <span className="num num-danger">9</span>
          </li>

          <li className="nav-item  avatar">
            <span className="person-avatar">
              <img src={AVATAR} alt="none" />
            </span>
            <span className="person-name">Alexander Pierce</span>
          </li>
          <li className="nav-item ">
            <i className="fa fa-cogs"></i>
          </li>
        </div>
      </div>
    </div>
  )
}

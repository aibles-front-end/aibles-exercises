import React, { useState } from 'react'
import Header from './components/Header/Header'
import HeaderContent from './components/HeaderContent/HeaderConTent'
import MainContent from './components/MainContent/MainContent'

import SideBar from './components/SideBar/SideBar'
import Footer from './components/Footer/Footer'
import './App.scss'
function App() {
  const [sidebar, setSidebar] = useState(true)

  return (
    <div className="App">
      <Header setSidebar={setSidebar} />

      <div className="wrapper">
        <div>
          <SideBar sidebar={sidebar}></SideBar>
        </div>
        <div className="home_mainContent">
          <HeaderContent></HeaderContent>
          <MainContent></MainContent>
          <Footer></Footer>
        </div>
      </div>
    </div>
  )
}

export default App

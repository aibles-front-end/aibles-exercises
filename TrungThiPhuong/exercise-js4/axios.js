

axios.defaults.baseURL = 'https://60becf8e6035840017c17a48.mockapi.io/api';

function getData() {
    axios.get('/users')
        .then(function (response) {
            appendData(response.data)
        })
        .catch(function (error) {
            console.log(error);
        });
}
getData()

function appendData(users) {
    const tbody = document.getElementById("tbody")
    tbody.innerHTML = ''
    
    users.map((user, index) =>
        tbody.innerHTML += (`
        <tr key=${index}>
            
            <th scope="row">${user.id}</th>
            <td>${user.name}</td>
            <td>${user.email}</td>
            <td>${user.city}</td>
            <td>
                <button  class="btn btn-primary mb-3" onclick="handleEdit(${user.id})">Edit</button>
            </td>
            <td>
                <button  class="btn btn-secondary mb-3" onclick="handleDetele(${user.id})">Detele</button>
            </td>
        </tr>
        `
        ))
}

const user = document.getElementsByClassName("form-control")

function handleAddUser() {
    const name = user[0].value
    const email = user[1].value
    const city = user[2].value
    if (name.length === 0 || email.length === 0 || city.length === 0) {
        alert("You must complete the input !!!")
    }
    else {
        axios.post('/users', {
            name: name,
            avatar: "https://cdn.fakercloud.com/avatars/d_kobelyatsky_128.jpg",
            email: email,
            city: city,
            image: "http://placeimg.com/640/480",
        })
            .then(function (response) {
                getData()
                alert(response.status)
            })
            .catch(function (error) {
                alert(error);
            });
    }
}
let userId;
function handleEdit(id){
 
    document.getElementById("button--save").style.display='block';
    document.getElementById("button--add").style.display = 'none';
  
    axios
      .get(`/users/${id}`)
      .then(d => {
        let customer = d.data;
        // console.log(customer);
        document.getElementById("name").focus();
        document.getElementById("name").value = customer.name;
        // console.log(document.getElementById("name"));
        document.getElementById("email").value = customer.email;
        document.getElementById("city").value = customer.city;
        
      })
      .catch(err => alert(err));
      userId=id;
     
      
}

function handleSave() {
    // idEdit=userId;
    console.log(userId);
    // console.log(user);
    const name = user[0].value
    const email = user[1].value
    const city = user[2].value
   
    if (name.length === 0 || email.length === 0 || city.length === 0) {
        alert("You must complete the input !!!")
    }
    else {
    axios.put(`/users/${userId}`,{
        name: name,
        email: email,
        city: city
    })
        .then(function (response) {
            getData()
            alert(response.status)
        })
        .catch(function (error) {
            alert(error);
        });
    }
    document.getElementById("button--save").style.display='none';
    document.getElementById("button--add").style.display = 'block';
}
function handleDetele(id){
    
    axios
      .delete(`/users/${id}`)
      .then(data => {
        console.log(data);
        getData();
      })
      .catch(err => alert(err));

}

function reverseWords(string){
    return string.split(' ').reverse().filter(s => s !== '').join(' ');
}
console.log(reverseWords('a  hello   world!  ')) ;

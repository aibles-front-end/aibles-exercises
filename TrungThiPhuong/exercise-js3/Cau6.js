const { default: axios } = require("axios");

function getByPromise () {
    axios.get('https://api.github.com/users/ptit9x')
        .then( values=> {
            console.log(values);
        })
        .catch( error=> {
            // handle error
            console.log(error);
        })
        .then(function () {
            // always executed
        });

   
}
getByPromise();

async function getByAsync () {
    try {
        const response = await axios.get('https://api.github.com/users/ptit9x');
        console.log(response);
    }
    catch (error) {
        console.error(error);
    }
}
// getByAsync();

